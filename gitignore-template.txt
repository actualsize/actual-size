# ignore Mac OS X Desktop Service Store
.DS_Store
# ignore debug.log
wp-content/debug.log
# ignore .htaccess
.htaccess
.htpasswd
#ignore uploads
wp-content/uploads/
# ignore backups
wp-content/backups/
#ignore upgrade
wp-content/upgrade/
#ignore wp-config
local-config.php
wp-config.php