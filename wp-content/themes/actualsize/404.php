<?php get_header(); ?>
	<div class="wrap">
		<h1 class="head-featured">Page not found</h1>
		<p>Sorry, the page you were looking for could not be found, or may never have existed.</p>
	</div>
<?php get_footer(); ?>