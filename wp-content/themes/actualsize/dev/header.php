<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<?php if (is_search()) { ?>
	<meta name="robots" content="noindex, nofollow" />
	<?php } ?>

	<title><?php

		global $page, $paged;

		wp_title( '|', true, 'right' );

		// Add the blog name.
		bloginfo( 'name' );

		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";

		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' | Page '. $paged;

	?></title>
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon/favicon.ico">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

	<script src="https://use.typekit.net/sek6ygc.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>

	<script src="<?php bloginfo('template_directory'); ?>/_/js/vendor/modernizr.custom.min.js"></script>
	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
	<header class="pageHeader" role="banner">
		<h1 class="logo"><a href="<?php echo get_option('home'); ?>/" class="ir" title="Actual Size"><span class="mark"></span><span class="visuallyhidden">Actual Size</span></a></h1>
		<nav id="nav" class="nav-holder" role="navigation">
			<?php  wp_nav_menu(array( 'menu' => 'main-navigation', 'menu_class'=>'main-navigation group' )); ?>
		</nav>
		<a href="#footer-nav" id="button-menu" class="menu icn">m</a>
	</header><!--/.pageHeader-->