<?php

/*
Template Name: Team
*/

?>
<?php get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
		$people = get_field('team_member', null);
?>

		<div class="team-intro">
			<h1 class="head-team">Meet <i>The Team</i></h1>
			<nav class="team-nav">
<?php
	if($people){
?>
				<ul>
<?php
		foreach($people as $person){
			$imgSm 	= $person['team_small_illustration'];
			$imgSm 	= $imgSm['sizes']['team-sm'];
?>
					<li><a href="#<?php echo $person['team_id']; ?>"><img src="<?php echo $imgSm; ?>"><?php echo $person['team_first_name']; ?></a></li>
<?php
		}
?>
				</ul>
<?php
	}
?>
			</nav>
		</div><!--/.team-intro-->
<?php

	if($people){

?>
		<ul class="team">
<?php
		foreach($people as $person){
			$firstName 	= $person['team_first_name'];
			$lastName 	= $person['team_last_name'];
			$id 		= $person['team_id'];
			$title 		= $person['team_title'];
			$bio 		= $person['team_bio'];
			$phone 		= $person['team_phone'];
			$ext 		= $person['team_extension'];
			$email 		= $person['team_email'];
			$quote 		= $person['team_quote'];

			$imgLrg 	= $person['team_large_illustration'];
			$imgLrg 	= $imgLrg['sizes']['team-lrg'];

?>

			<li class="vcard team-member" id="<?php echo $id; ?>">
				<div class="team-member-inner" style="background-image: url('<?php echo $imgLrg; ?>');">
					<div class="detail">
						<h2 class="head-bio"><i class="fn"><?php echo $firstName.' '.$lastName; ?></i> <i class="title"><?php echo $title; ?></i></h2>
						<div class="bio">
							<?php echo $bio; ?>
						</div><!--/.bio-->

						<span class="tel"><?php echo $phone; ?> x <?php echo $ext; ?></span>
<?php
						$emailLink = '<a href="mailto:'.$email.'" class="email">'.$email.'</a>';
						echo enkode($emailLink);
?>

<?php
					if($quote){
?>
						<blockquote class="bio-quote">“<?php echo $quote; ?>”</blockquote>
<?php
					}
?>
					</div><!--/.detail-->
				</div><!--/.team-member-inner-->
			</li><!--/.team-member-->
<?php
		}
?>
		</ul><!--/.team-->
<?php
	}
?>




	<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>
