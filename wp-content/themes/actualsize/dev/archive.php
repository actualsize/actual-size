<?php get_header(); ?>

<?php if (have_posts()) : ?>

	<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

	<?php /* If this is a category archive */ if (is_category()) { ?>
		<h1 class="headTitle pagehead-b teal">Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h1>

	<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
		<h1 class="headTitle pagehead-b teal">Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h1>

	<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		<h1 class="headTitle pagehead-b teal">Archive for <?php the_time('F jS, Y'); ?></h1>

	<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		<h1 class="headTitle pagehead-b teal">Archive for <?php the_time('F, Y'); ?></h1>

	<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		<h1 class="headTitle pagehead-b teal">Archive for <?php the_time('Y'); ?></h1>

	<?php /* If this is an author archive */ } elseif (is_author()) { ?>
		<h1 class="headTitle pagehead-b teal">Author Archive</h1>

	<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		<h1 class="headTitle pagehead-b teal">Blog Archives</h1>

	<?php } ?>

<?php endif; ?>



	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<h1><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>

			<?php include (TEMPLATEPATH . '/_/inc/meta.php' ); ?>

			<div class="entry">
				<?php custom_excerpt('custom_excerpt_length', 'custom_excerpt_more'); ?>
			</div>

			<a href="<?php the_permalink(); ?>">Read More</a>

			<footer class="postmetadata">
				<?php the_tags('Tags: ', ', ', '<br />'); ?>
				<?php	/* Posted in <?php the_category(', ') ?> | */ ?>
				<?php /* comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); */ ?>
			</footer>

		</article>

	<?php endwhile; ?>

	<?php include (TEMPLATEPATH . '/_/inc/nav.php' ); ?>

	<?php else : ?>

		<h2>Not Found</h2>

	<?php endif; ?>


<?php get_sidebar(); ?>

<?php get_footer(); ?>
