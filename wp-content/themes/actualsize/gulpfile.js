// Include plugins ===================================================================
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var prettify = require('gulp-prettify');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var browserSync = require('browser-sync');


// Browser Sync ======================================================================
gulp.task('sync', function() {
  browserSync({
    proxy: "actualsizedev.com:8888"
  })
});



// HTML ==============================================================================
gulp.task('php', function () {
  gulp.src('dev/*.php')
    .pipe(gulp.dest(''))
    // .pipe(browserSync.reload({
    //   stream: true
    // }));
});



// CSS ===============================================================================
gulp.task('css', function() {
  return gulp.src('dev/scss/*.scss')
    .pipe(sass()).on('error', sass.logError)
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(cssmin({
      showLog: true,
      debug: true,
      roundingPrecision: true,
      keepSpecialComments: false
    }))
    .pipe(rename('style.css'))
    .pipe(gulp.dest(''))
    // .pipe(browserSync.reload({
    //   stream: true
    // }));
});





// Watch =============================================================================
gulp.task('watch', ['sync'], function() {
  gulp.watch('dev/*.php', ['php']);
  gulp.watch('dev/scss/*.scss', ['css']);
});



// Default Task ======================================================================
gulp.task('default', ['watch']);