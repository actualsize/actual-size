<?php

/* 
Template Name: Project List
*/

?>
<?php get_header(); ?>
	
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
	
		$headImgLrg 	= get_field('home_head_image_lrg', null); 
		$headImgSm 		= get_field('home_head_image_sm', null); 
		
		$headOuterBg	= get_field('head_outer_background_color', null);
		
		$altText 		= get_field('header_alt_text', null); 
		
		if($headImgLrg && $headImgSm){
			$headImgLrg 	= $headImgLrg['sizes']['full'];
			$headImgSm 		= $headImgSm['sizes']['full-sm'];
?>
		<div class="full spc-btm" <?php if($headOuterBg) { ?> style="background: <?php echo $headOuterBg; ?>;" <?php } ?>>
			<div data-picture data-alt="<?php echo $altText; ?>">
				<div data-src="<?php echo $headImgSm; ?>"></div>
				<div data-src="<?php echo $headImgLrg; ?>" data-media="(min-width: 768px)"></div>
				<noscript><img src="<?php echo $headImgLrg; ?>" alt="<?php echo $altText; ?>" ></noscript>
			</div>
		</div><!--/.full-->
<?php			
		}
?>	

<?php endwhile; ?>
<?php endif; ?>	
	
	
		<section class="projects">
<?php
		$args=array(
			'post_type' => 'projects',
	    	'posts_per_page'=> 33,
	    	'orderby'   => 'menu_order',
            'order'     => 'ASC'
		);
	
		$projects = null;
		$projects = new WP_Query($args);
	
		if($projects->have_posts()) {
?>
			<ul class="project-list sm">
<?php	
			while ($projects->have_posts()) : $projects->the_post();
				$thumb			= get_field('list_thumbnail', null);
				$thumb			= $thumb['sizes']['grid-thumb'];
				$client 		= get_field('client', null);  
?>
				<li>
					<a href="<?php the_permalink(); ?>">
<?php
				if($thumb){
?>
					<img src="<?php echo $thumb; ?>">
<?php						
						
				} else {
?>	
					<img src="<?php bloginfo('template_directory'); ?>/img/thumb-blank.gif">
<?php					
				}
?>					
						<h2 class="head-item"><?php the_title(); ?>&nbsp;<i class="arw"></i></h2>
					</a>
<?php
				if($client){	
					$post = $client;
					setup_postdata($post);	
					$hasDetail	= get_field('has_detail_page', $post->ID);

					if($hasDetail == 'yes'){
?>
						<a href="<?php echo the_permalink(); ?>" class="sub-item"><?php echo the_title(); ?></a>
<?php						
					} else {
?>
						<span class="sub-item"><?php echo the_title(); ?></span>
<?php						
					}	
					
					wp_reset_postdata();	
				}
?>					
				</li>
<?php			
		
		
			endwhile;
	
?>
			</ul><!-- /.project-list-->
<?php		
	} 
?>	

		</section><!--/.projects-->

		<?php include('_/inc/featured-clients.php'); ?>

<?php get_footer(); ?>
