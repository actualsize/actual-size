<?php get_header(); ?>
<h1>Search Results</h1>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<article <?php post_class() ?> id="post-
   <?php the_ID(); ?>">
   <h1><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
   <?php //include (TEMPLATEPATH . '/_/inc/meta.php' ); ?>
   <div class="entry">
      <?php //custom_excerpt('custom_excerpt_length', 'custom_excerpt_more'); ?> </div>
</article>
<?php endwhile; ?>
<?php include (TEMPLATEPATH . '/_/inc/nav.php' ); ?>
<?php else : ?>
<p>Sorry, there were no results found. Please refine your search and try again.</p>
<?php endif; ?>
<?php get_footer(); ?>