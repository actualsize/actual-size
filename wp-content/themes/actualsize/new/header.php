<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?>>
   <!--<![endif]-->

   <head>
      <meta charset="<?php bloginfo('charset'); ?>">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <?php if (is_search()) { ?>
      <meta name="robots" content="noindex, nofollow" />
      <?php } ?>
      <title>
         <?php
		global $page, $paged;
		wp_title( '|', true, 'right' );
		bloginfo( 'name' );
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
		if ( $paged >= 2 || $page >= 2 )
			echo ' | Page '. $paged;
	?>
      </title>
      <meta name="description" content="<?php bloginfo('description'); ?>">
      <meta name="viewport" content="width=device-width">
      <link rel="shortcut icon" href="/wp-content/themes/actual-size/img/favicon/favicon.ico">
      <link rel="stylesheet" href="/wp-content/themes/actual-size/style.css">
      <!--[if lte IE 8]><link rel="stylesheet" href="/wp-content/themes/actual-size/css/ie8.css"><![endif]-->
      <!-- LIVE ====== -->
      <!-- <script type="text/javascript" src="//use.typekit.net/non4hix.js"></script> -->
      <!-- <script type="text/javascript">try{Typekit.load();}catch(e){}</script> -->
      <!-- LIVE ====== -->
      <!-- LOCAL ====== -->
      <script src="https://use.typekit.net/sek6ygc.js"></script>
      <script>
         try
         {
            Typekit.load(
            {
               async: true
            });
         }
         catch (e)
         {}
      </script>
      <!-- LOCAL ====== -->
      <script src="/wp-content/themes/actual-size/_/js/vendor/modernizr.custom.min.js"></script>
   </head>

   <body <?php body_class(); ?>>
      <header class="pageHeader" role="banner">
         <h1 class="logo"><a href="/" class="ir" title="Actual Size"><span class="mark"></span><span class="visuallyhidden">Actual Size</span></a></h1>
         <nav id="nav" class="nav-holder" role="navigation">
            <?php  wp_nav_menu(array( 'menu' => 'main-navigation', 'menu_class'=>'main-navigation group' )); ?> </nav> <a href="#footer-nav" id="button-menu" class="menu icn">m</a> </header>
      <!--/.pageHeader-->