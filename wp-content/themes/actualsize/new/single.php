<?php get_header(); ?>
<div class="blog-outer">
   <?php 
	if (have_posts()) : while (have_posts()) : the_post(); ?>
   <?php
		$post_id = $post->ID;
?>
      <article <?php post_class() ?> id="post-
         <?php the_ID(); ?>">
         <div class="post-featured-img">
            <?php 
			$image = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'full');

			if($image){
?> <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>">
            <?php														
			} 
?>
         </div>
         <div class="post-detail">
            <div class="inner">
               <h1 class="head-block d cCoral"><?php the_title(); ?></h1>
               <div class="post-content">
                  <?php the_content(); ?> </div>
            </div>
            <!--/.inner-->
            <?php
				$author_id = $post->post_author;
				$author_headshot	= get_field('user_headshot', 'user_'.$author_id);
				$author_color 		= get_field('user_color', 'user_'.$author_id);
				$author_anchor 		= get_field('user_anchor', 'user_'.$author_id);
?>
               <a href="<?php echo get_permalink(168); echo '#'.$author_anchor; ?>" class="post-author">
                  <?php 
					if($author_headshot){
						$author_headshot = $author_headshot['sizes']['team-headshot'];
?>
                  <div class="author-img"> <img src="<?php echo $author_headshot; ?>"> </div>
                  <?php					
					}
					
?> From The Desk Of <span class="author-name" style="color: <?php echo $author_color; ?>;"><?php echo get_the_author(); ?></span> </a>
               <!--/.post-author-->
         </div>
         <!--/.post-detail-->
         <footer class="post-date"> Published <b><?php the_time('F j, Y') ?></b> </footer>
      </article>
      <?php endwhile; ?>
      <?php else : ?>
      <div class="row low post-detail clipped">
         <div class="col-10">
            <h2 class="head-geo">Page Not Found</h2>
            <div class="content-generic xl">
               <p>Sorry, the page you were looking for could not be found, or may never have existed.</p>
               <p>If you’d like, you can try <a href="#searchform" class="scroll" style="text-decoration: underline;">searching</a> for it.</p>
               <p>&nbsp;</p>
               <p>&nbsp;</p>
            </div>
            <!--/.content-generic-->
         </div>
      </div>
      <?php endif; ?>
      <?php
	$exclude_ids = array($post_id);
	
	$args=array(
		'post_type' => 'post',
		'posts_per_page'=> 3,
		'orderby'   => 'date',
		'order'     => 'DESC',
		'post__not_in'	=> $exclude_ids
	);
	
	$more_posts = null;
	$more_posts = new WP_Query($args);
	
	if($more_posts->have_posts()) {

?>
         <aside class="more-posts">
            <div class="inner">
               <h1 class="head-srf a cntr lined shallow"><span class="txt single">More Stuff W<i class="krn-6-l">e</i> W<i class="krn-6-l">r</i>ote <span class="lrg-only">for You</span></span></h1>
               <div class="list-posts">
                  <?php
				while ($more_posts->have_posts()) : $more_posts->the_post();
					$author_id 			= $post->post_author;
					$author_color 		= get_field('user_color', 'user_'.$author_id);
					$author_anchor 		= get_field('user_anchor', 'user_'.$author_id);
					$post_thumb 		  = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'team-headshot');				

?>
                     <article class="post-tease">
                        <div class="img">
                           <?php
						if($post_thumb){
?> <a href="<?php the_permalink(); ?>"><img src="<?php echo $post_thumb[0]; ?>"></a>
                              <?php				
						} else {
?> &nbsp;
                                 <?php				
						}
?>
                        </div>
                        <!_-/.img-->
                        <div class="detail">
                           <h1 class="head-thin"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                           <a href="<?php the_permalink(); ?>">
                              <div class="content">
                                 <?php
									global $more; 
									$more = false; 
									the_excerpt();
									$more = true;
?>
                              </div>
                              <!--/.content-->
                           </a> <a href="<?php echo get_permalink(168); echo '#'.$author_anchor; ?>" class="tease-author">From The Desk Of <span class="author-name" style="color: <?php echo $author_color; ?>;"><?php echo get_the_author(); ?></span></a> </div>
                        <!--/.detail-->
                     </article>
                     <!--/.post-tease-->
                     <?php
				endwhile;
?>
               </div>
               <!--/.list-posts-->
            </div>
            <!--/.inner-->
         </aside>
         <!--/.more-posts-->
         <?php

	}
?>
</div>
<!--/.blog-outer-->
<?php get_sidebar(); ?>
<?php get_footer(); ?>