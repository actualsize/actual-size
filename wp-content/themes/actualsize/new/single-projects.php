<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php	
		$hero 		= get_field('hero_image', null); 
		if($hero){
			$hero 		= $hero['sizes']['full'];
		}
		$imgs		= get_field('case_study_images', null); 
		
		$services 	= get_field('what_we_did', null); 
		$link 		= get_field('website_url', null); 
		$client 	= get_field('client', null); 
		$next_post 	= get_adjacent_post(false,'',false);
		$prev_post	= get_adjacent_post(false,'',true);
?>
   <section class="project" role="main">
      <div class="hero-wrap">
         <div class="row-outer">
            <?php
				if($hero){
?> <img src="<?php echo $hero; ?>" class="hero" alt="<?php the_title(); ?>">
               <?php
				}
?>
         </div>
      </div>
      <header class="project-header">
         <div class="title-wrap group">
            <div class="inner"> <a href="<?php echo get_home_url(); ?>" class="eye icn">a</a>
               <div class="title">
                  <h1 class="head-block c project-title"><?php the_title(); ?></h1>
                  <?php
					if($client){	
						$post = $client;
						setup_postdata($post);	
						$hasDetail	= get_field('has_detail_page', $post->ID);
						if($hasDetail =='yes'){
?>
                     <a href="<?php echo the_permalink(); ?>" class="sub-item client">
                        <?php echo the_title(); ?>&nbsp;<i class="arw"></i></a>
                     <?php						
						} else {
?>
                        <div class="sub-item client">
                           <?php echo the_title(); ?>
                        </div>
                        <?php						
						}	
						
						wp_reset_postdata();
					}
?>
               </div>
               <!--/.title-->
               <div class="nav-projects">
                  <a href="<?php echo get_permalink($prev_post->ID); ?>" id="prev-project">
                     <</a> <a href="<?php echo get_permalink($next_post->ID); ?>" id="next-project">></a> </div>
            </div>
            <!--/.inner-->
         </div>
         <!--/.title-wrap-->
      </header>
      <!--/.project-header-->
      <div class="outer project-info-outer">
         <div class="project-info">
            <div class="imgs">
               <?php
			if($imgs){
				foreach($imgs as $img){
					
					$item = $img['case_study_image'];
					
					if($item){
						$item 		=  $item['sizes']['detail'];
						$itemSize 	=  getimagesize($item);
					} else {
						$itemSize 	= null;
					}
?>
                  <figure class="project-img<?php if($img['case_study_flush_bottom']=='yes') echo ' flush-btm'; if($img['case_study_flush_top']=='yes') echo ' flush-top'; ?>">
                     <figcaption class="caption">
                        <?php echo $img['case_study_caption']; ?>
                     </figcaption>
                     <?php					
					if($img['case_study_media_type'] != 'vid'){
?> <img class="lazy" src="<?php bloginfo('template_directory'); ?>/img/b.gif" data-original="<?php echo $item; ?>" width="<?php echo $itemSize[0]; ?>" height="<?php echo $itemSize[1]; ?>"> <noscript><img src="<?php echo $item; ?>"></noscript>
                        <?php							
					} else {
						$vid_mp4 = $img['video_url_mp4'];
						$vid_webm = $img['video_url_webm'];
?> <video width="870" height="489" class="vidPlayer" poster="<?php echo $item; ?>" controls="controls" preload="none">
<?php
						if($vid_mp4){
?>
							<source type="video/mp4" src="http://dbjw368sojxbt.cloudfront.net/<?php echo $vid_mp4; ?>" />
<?php							
						}
							
						if($vid_webm){
?>
							<source type="video/webm" src="http://dbjw368sojxbt.cloudfront.net/<?php echo $vid_webm; ?>" />
<?php								
						}
?>						
						    <object width="870" height="489" type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/_/swf/flashmediaelement.swf">
						        <param name="movie" value="flashmediaelement.swf" />
<?php
						if($vid_mp4){
?>						        
						        <param name="flashvars" value="controls=true&file=http://dbjw368sojxbt.cloudfront.net/<?php echo $vid_mp4; ?>" />
<?php
						}
?>						        
						        <img src="<?php echo $item; ?>" width="870" height="489" title="Can't Load Video" />
						    </object>
						</video>
                           <?php						
					}
?>
                  </figure>
                  <?php					
				}
			}
?>
            </div>
            <!--/.imgs-->
            <div class="project-detail">
               <div class="desc">
                  <?php the_content(); ?> </div>
               <!--/.desc-->
               <div class="what-we-did">
                  <?php
					if($services){
?>
                     <h2 class="head-block">What We Did</h2>
                     <ul>
                        <?php					
						foreach($services as $service){
?>
                           <li>
                              <?php echo $service['service']; ?>
                           </li>
                           <?php						
						}	
?>
                     </ul>
                     <?php										
					}
?>
               </div>
               <!--/.what-we-did-->
               <?php				
					if($link){
?> <a href="<?php echo forceURL($link); ?>" class="button-view-site" target="_blank">View&nbsp;Site</a>
                  <?php								
					}	
	
				if($client){
					
					$post = $client;
					setup_postdata($post);	
					$hasDetail	= get_field('has_detail_page', $post->ID);
					if($hasDetail =='yes'){		
?>
                     <hr>
                     <aside class="client-more">
                        <h1 class="head-client-more">Wanna see more projects for this client?</h1> <a href="<?php echo the_permalink(); ?>" class="button-block view-client">View Client Page&nbsp;<i class="arw"></i></a> </aside>
                     <?php						
					}
					
					wp_reset_postdata();
				}
?>
            </div>
            <!--/.detail-->
         </div>
         <!--/.project-info-->
      </div>
      <!--/.project-info-outer-->
   </section>
   <!--/.project-->
   <?php endwhile; ?>
   <?php endif; ?>
   <?php include('_/inc/more-projects.php'); ?>
   <?php get_footer(); ?>