<?php

/* 
Template Name: Home
*/

?>
   <?php get_header(); ?>
   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
   <?php
	
		$headImgLrg 	= get_field('home_head_image_lrg', null); 
		$headImgLrg 	= $headImgLrg['sizes']['full'];
				
		$headImgSm 		= get_field('home_head_image_sm', null); 
		$headImgSm 		= $headImgSm['sizes']['full-sm'];
		
		$headOuterBg	= get_field('head_outer_background_color', null);
		
		$altText 		= get_field('header_alt_text', null); 
		
		$headImgCaption	= get_field('home_head_caption', null);
		
		if($headImgLrg && $headImgSm){
?>
      <div class="full" <?php if($headOuterBg) { ?> style="background:
         <?php echo $headOuterBg; ?>;"
         <?php } ?>>
         <div data-picture data-alt="<?php echo $altText; ?>">
            <div data-src="<?php echo $headImgSm; ?>"></div>
            <div data-src="<?php echo $headImgLrg; ?>" data-media="(min-width: 768px)"></div> <noscript><img src="<?php echo $headImgLrg; ?>" alt="<?php echo $altText; ?>" ></noscript> </div>
         <?php
		if($headImgCaption){
?>
            <div class="caption-full">
               <?php echo $headImgCaption; ?>
            </div>
            <?php			
		}
?>
      </div>
      <!--/.full-->
      <?php			
		}
?>
         <section>
            <h1 class="head-block full">Featured Projects&nbsp;<i class="arw dwn"></i></h1>
            <?php
		$projects 	= get_field('home_projects', null); 
		if($projects){
?>
               <ul class="featured-projects">
                  <?php
			foreach($projects as $project){
				$projectObj = $project['featured_project'];
				$imgLrg 	= $project['featured_img_lrg']['sizes']['full'];
				$imgSm 		= $project['featured_img_sm']['sizes']['grid-thumb'];
				$outerBg	= $project['featured_outer_background_color'];
				
				
				if($projectObj){
					$post = $projectObj;
					setup_postdata($post);
					$client = get_field('client', $post->ID); 
?>
                     <li style="background:<?php echo $outerBg; ?>;"> <a href="<?php the_permalink(); ?>" class="featured-project">
						<div data-picture data-alt="<?php the_title(); ?>">
<?php
						if($imgLrg && $imgSm){
?>												
							<div data-src="<?php echo $imgSm; ?>"></div>
							<div data-src="<?php echo $imgLrg; ?>" data-media="(min-width: 768px)"></div>
							<noscript><img src="<?php echo $imgLrg; ?>" alt="<?php the_title(); ?>"></noscript>
<?php
						}
?>							
						</div>
						<div class="inner">
							<div class="row">
								<div class="detail<?php if($project['featured_caption_position'] == 'right') { echo ' rt';} if($project['featured_text_color'] == 'drk'){ echo ' drk'; }?>">
									<h2 class="head-featured"><?php the_title(); ?>&nbsp;<i class="arw"></i></h2>
<?php
			if($client){	
				$post = $client;
				setup_postdata($post);	
?>
				<div class="client"><?php echo the_title(); ?></div>
<?php					
				wp_reset_postdata();
			}
?>									
									
									<div class="desc">
										<?php echo $project['featured_desc']; ?>
									</div><!--/.desc-->
								</div><!--/.detail-->
							</div><!--/.row-->
						</div><!--.inner-->
					</a>
                        <!--/.featured-project-->
                     </li>
                     <?php				
					wp_reset_postdata();
				}
			}
?>
               </ul>
               <!--/featured-projects-->
               <?php				
		}
?>
         </section>
         <?php endwhile; ?>
         <?php endif; ?>
         <?php include('_/inc/featured-clients.php'); ?>
         <?php get_footer(); ?>