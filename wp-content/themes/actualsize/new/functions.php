<?php
	if(!isset($content_width)){
     	$content_width = 870;
    }

    /* ========================================================================================================================
		Theme Setup
	======================================================================================================================== */

	if ( ! function_exists( 'custom_theme_setup' ) ):
		function custom_theme_setup() {
			 if ( function_exists( 'add_theme_support' ) ) {
			 	 // Add 3.1 post format theme support.
			 	 add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'audio', 'chat', 'video'));
				 // Enable support for Post Thumbnails
				 add_theme_support('post-thumbnails');
				 set_post_thumbnail_size(220,153,TRUE);
			 }

			 if ( function_exists( 'add_image_size' ) ) {
				 add_image_size( 'full', 1500, 9999);
				 add_image_size( 'large', 1100, 9999);
				 add_image_size( 'detail', 870, 9999);
				 add_image_size( 'grid-thumb', 640, 552, TRUE );
				 add_image_size( 'full-sm', 640, 9999);
				 add_image_size( 'client-hero', 640, 9999);
				 add_image_size( 'admin-preview', 780, 9999);
				 add_image_size( 'team-sm', 81, 9999);
				 add_image_size( 'team-lrg', 667, 9999);
				 add_image_size( 'team-headshot', 220, 220, TRUE);
				 add_image_size( 'process-step', 450, 450, TRUE);
			 }

			 if ( function_exists( 'register_nav_menus' ) ) {
				  // Register Navigation
			 register_nav_menus(array('main-navigation' => __( 'Main Navigation' ), 'secondary-navigation' => __( 'Secondary Navigation' )));
			 }

			 // Custom Theme Options
			 require( get_template_directory() . '/_/inc/theme-options.php' );
		}
	endif; // custom_theme_setup

	/* ========================================================================================================================
		Function Definitions
	======================================================================================================================== */

	// Load jQuery
	function my_jquery_enqueue() {
		if(!is_page(1345)){
			wp_deregister_script('jquery');
			wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js", false, null, true);
			wp_enqueue_script('jquery');
		}
	}

	// Put CSS in it's own directory
	function change_css_path() {
		return get_bloginfo('template_url') . '/css/style.css';
	}

	//Remove Navigation Containers
	function my_wp_nav_menu_args( $args = '' ){
		$args['container'] = false;
		return $args;
	} // function

	//Add Home Button to Nav
	function home_page_menu_args( $args ) {
		$args['show_home'] = true;
		return $args;
	}

	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head','rsd_link');
    	remove_action('wp_head','wlwmanifest_link');
    	remove_action('wp_head','wp_generator');
    	remove_action('wp_head','feed_links_extra', 3 );
    	remove_action('wp_head','feed_links', 2);
    	remove_action('wp_head','rel_canonical');
		remove_action('wp_head','adjacent_posts_rel_link_wp_head', 10, 0 );
		remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
    }

    //Allow test for post type outside the loop
    function is_post_type($type){
	    global $wp_query;
	    if($type == get_post_type($wp_query->post->ID)) return true;
	    return false;
	}

	//Custom Excerpts
	function custom_excerpt_length_home($length) {
		return 20;
	}

	function custom_excerpt_length($length) {
		return 50;
	}

	function custom_excerpt_more($more) {
		return '...';
	}


	function custom_excerpt($length_callback='', $more_callback='') {
	    global $post;
	    if(function_exists($length_callback)){
	        add_filter('excerpt_length', $length_callback);
	    }
	    if(function_exists($more_callback)){
	        add_filter('excerpt_more', $more_callback);
	    }
	    $output = get_the_excerpt();
	    $output = apply_filters('wptexturize', $output);
	    $output = apply_filters('convert_chars', $output);
	    $output = '<p>'.$output.'</p>';
	    echo $output;
	}

	function posts_link_attributes_newer() {
	    return 'class=""';
	}

	function posts_link_attributes_older() {
	    return 'class=""';
	}

	function the_slug() {
		$post_data = get_post($post->ID, ARRAY_A);
		$slug = $post_data['post_name'];
		return $slug;
	}

	function SearchFilter($query) {
	    if ($query->is_search) {
	    	$searchArgs=array(
				'post',
				'page'/*,
				'custom-post-type-name'
				*/
			);
	        $query->set('post_type', $searchArgs);
	    }
	    return $query;
	}

	 function theme_widgets_init() {
		register_sidebar( array(
			'name' => __( 'Sidebar', 'actual_size' ),
			'id' => 'sidebar-1',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => "</aside>",
			'before_title' => '<h1 class="widget-title">',
			'after_title' => '</h1>',
		) );
	}

	// Function for trimming "http://" from beginning of URL
	function formatURL($url){
		if(substr($url, 0, 7) === "http://"){
			$url = substr($url, 7);
		}
		return $url;
	}

	// Function for placing "http://" on the beginning of URL
	function forceURL($url){
		if(substr($url, 0, 7) !== "http://"){
			$url = "http://".$url;
		}
		return $url;
	}

	add_filter( 'protected_title_format', 'remove_protected_text' );
	function remove_protected_text() {
		return __('%s');
	}

	add_shortcode( 'button' , 'button_shortcode');

	function button_shortcode($atts) {
    	$class = "button-block";

    	if($atts['type'] == 'blend'){
        	$class .= " blend";
    	} elseif($atts['type'] == 'white'){
        	$class .=" lt";
    	}

        return '<a href="'.$atts['url'].'" class="'.$class.'">'.$atts['text'].'&nbsp;<i class="arw"></i></a>';
    }


	/* ========================================================================================================================
	Actions and Filters
	======================================================================================================================== */

	if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
	add_action('after_setup_theme', 'custom_theme_setup' );
	add_action('init', 'removeHeadLinks' );
    add_action('widgets_init', 'theme_widgets_init' );
    add_filter('pre_get_posts','SearchFilter');
    add_filter('stylesheet_uri', 'change_css_path' );
    add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args' );
    add_filter('wp_page_menu_args', 'home_page_menu_args' );
    add_filter('next_posts_link_attributes', 'posts_link_attributes_newer');
	add_filter('previous_posts_link_attributes', 'posts_link_attributes_older');