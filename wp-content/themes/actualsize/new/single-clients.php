<?php if (have_posts()) : while (have_posts()) : the_post(); 
	$hasDetail	= get_field('has_detail_page', $post->ID);
	if($hasDetail != 'yes'){
		wp_redirect(site_url().'/work');
	}
?>
<?php endwhile; ?>
<?php endif; ?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php
		$hero 		= get_field('client_hero', null); 
		$hero 		= $hero['sizes']['client-hero'];
		$projects	= get_field('client_projects', null);

?>
   <section class="client">
      <header class="client-header">
         <div class="inner">
            <div class="img">
               <?php
					if($hero){
?> <img src="<?php echo $hero; ?>" alt="<?php the_title(); ?>">
                  <?php
					}
?>
            </div>
            <div class="detail">
               <h1 class="head-geo"><?php the_title(); ?></h1>
               <div class="desc">
                  <?php the_content(); ?> </div>
               <!--/.desc-->
            </div>
            <!--/.detail-->
         </div>
         <!--/.inner-->
      </header>
      <!--/.client-header-->
      <?php
if($projects){
	$count = count($projects);
	$i = 1;
	foreach($projects as $project){

			$link = null;
			
			if($project['client_project_link_type'] == 'project'){
				$projectObj = $project['client_project'];
				$post = $projectObj;
				setup_postdata($post);
				
				$link = get_the_permalink();
				wp_reset_postdata();
				
			} elseif($project['client_project_link_type'] == 'custom'){
				$link = $project['client_project_custom_link'];
			}
			
?>
         <div class="project-info per-client <?php if($i == $count) echo 'last'; ?>">
            <div class="imgs">
               <?php
						$item 		=  $project['client_project_image']['sizes']['detail'];
						if($item){
							$itemSize 	=  getimagesize($item);
						} else {
							$itemSize = null;
						}						
?>
                  <figure class="project-img">
                     <?php 
						if($project['client_project_caption']){
?>
                     <figcaption class="caption">
                        <?php echo $project['client_project_caption']; ?>
                     </figcaption>
                     <?php
						} else {
?>
                        <div class="caption">&nbsp;</div>
                        <?php							
						}
						
						if($project['project_media_type'] != 'vid'){
?>
                           <?php
							if($link){
?>
                              <a href="<?php echo $link; ?>">
                                 <?php
							}
?> <img class="lazy" src="<?php bloginfo('template_directory'); ?>/img/b.gif" data-original="<?php echo $item; ?>" width="<?php echo $itemSize[0]; ?>" height="<?php echo $itemSize[1]; ?>"> <noscript><img src="<?php echo $item; ?>"></noscript>
                                    <?php
							if($link){
?>
                              </a>
                              <?php
							}
?>
                                 <?php							
						} else {
							$vid_mp4 = $project['project_video_file_mp4'];
							$vid_webm = $project['project_video_file_webm'];
?> <video width="870" height="489" class="vidPlayer" poster="<?php echo $item; ?>" controls="controls" preload="none">
<?php
							if($vid_mp4){
?>
								<source type="video/mp4" src="http://dbjw368sojxbt.cloudfront.net/<?php echo $vid_mp4; ?>" />
<?php							
							}
								
							if($vid_webm){
?>
								<source type="video/webm" src="http://dbjw368sojxbt.cloudfront.net/<?php echo $vid_webm; ?>" />
<?php								
							}
?>						
							    <object width="870" height="489" type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/_/swf/flashmediaelement.swf">
							        <param name="movie" value="flashmediaelement.swf" />
<?php
							if($vid_mp4){
?>						        
							        <param name="flashvars" value="controls=true&file=http://dbjw368sojxbt.cloudfront.net/<?php echo $vid_mp4; ?>" />
<?php
							}
?>						        
							        <img src="<?php echo $item; ?>" width="870" height="489" title="Can't Load Video" />
							    </object>
							</video>
                                    <?php							
						}						
						
						
?>
                  </figure>
            </div>
            <!--/.imgs-->
            <div class="project-detail">
               <?php
					if($link){
?>
                  <h2 class="head-block b"><a href="<?php echo $link; ?>"><?php echo $project['client_project_title']; ?></a></h2>
                  <?php						
					} else {
?>
                     <h2 class="head-block b"><?php echo $project['client_project_title']; ?></h2>
                     <?php						
					}
?>
                        <div class="desc">
                           <?php echo $project['client_project_description']; ?> </div>
                        <!--/.desc-->
                        <?php
					if($link){
?> <a href="<?php echo $link; ?>" class="button-block blend">View Project&nbsp;<i class="arw"></i></a>
                           <?php						
					} 
?>
            </div>
            <!--/.detail-->
         </div>
         <!--/.project-info-->
         <?php			
			
			
		
		$i++;
	}
}
?>
   </section>
   <!--/.client-->
   <?php endwhile; ?>
   <?php endif; ?>
   <?php include('_/inc/featured-clients.php'); ?>
   <?php get_footer(); ?>