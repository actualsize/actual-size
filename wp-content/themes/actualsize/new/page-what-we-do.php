<?php
/* 
Template Name: What We Do
*/
?>
   <?php get_header(); ?>
   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
   <div class="what-we-do-top">
      <div class="what-we-do-overlay default">
         <div class="inner">
            <h1 class="head-what-we-do">
						<div class="line-1">The state of design and technology is always moving.</div>
						<div class="line-2">so are we</div>
					</h1> </div>
      </div>
      <!--/.what-we-do-overlay--><video width="1844" height="576" id="timelapse" poster="<?php bloginfo('template_directory'); ?>/img/what-we-do-poster.jpg" preload="none" muted loop autoplay webkit-playsinline>
				<source type="video/mp4" src="http://dbjw368sojxbt.cloudfront.net/timelapse.mp4" />
				<source type="video/webm" src="http://dbjw368sojxbt.cloudfront.net/timelapse.WebM" />
			
			    <object width="1844" height="576" type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/_/swf/flashmediaelement.swf">
			        <param name="movie" value="flashmediaelement.swf" />
			        
			        <param name="flashvars" value="controls=true&file=http://dbjw368sojxbt.cloudfront.net/timelapse.mp4" />
			        
			        <img src="<?php bloginfo('template_directory'); ?>/img/what-we-do-poster.jpg" width="1844" height="576" title="Can't Load Video" />
			    </object>
			</video> </div>
   <div class="bgDark">
      <section class="capabilities">
         <div class="inner">
            <h1 class="head-srf a cCoral cntr"><span class="word-1">Specifically,</span> this is what we do</h1>
            <?php
			$serviceCategories = get_field('service_category', null);
			
			if($serviceCategories){
				foreach($serviceCategories as $category){
					$services = $category['services'];
?>
               <div class="services">
                  <h2 class="head-block"><?php echo $category['service_category_name']; ?></h2>
                  <?php
					if($services){
?>
                     <ul class="list-geo">
                        <?php				
						foreach($services as $service){
?>
                           <li>
                              <?php echo $service['service']; ?>
                           </li>
                           <?php						
						}
?>
                     </ul>
                     <?php					
					}
?>
               </div>
               <?php			
				}
			} 
?>
         </div>
      </section>
   </div>
   <!--/.what-we-do-btm-->
   <section class="process">
      <h1 class="head-srf b cCoral cntr">About the process</h1>
      <?php
				$step_1_title 	= get_field('step_1_title', null);
				$step_1_img 	= get_field('step_1_image', null);
				$step_1_img 	= $step_1_img['url'];
				$step_1_desc 	= get_field('step_1_description', null);
				
				$step_2_title 	= get_field('step_2_title', null);
				$step_2_img 	= get_field('step_2_image', null);
				$step_2_img 	= $step_2_img['url'];
				$step_2_desc 	= get_field('step_2_description', null);
				
				$step_3_title 	= get_field('step_3_title', null);
				$step_3_img 	= get_field('step_3_image', null);
				$step_3_img 	= $step_3_img['url'];
				$step_3_desc 	= get_field('step_3_description', null);
				
				$step_4_title 	= get_field('step_4_title', null);
				$step_4_img 	= get_field('step_4_image', null);
				$step_4_img 	= $step_4_img['url'];
				$step_4_desc 	= get_field('step_4_description', null);
				
?>
         <div class="row">
            <div class="process-step"> <img src="<?php echo $step_1_img; ?>">
               <h2 class="head-block"><?php echo $step_1_title; ?></h2>
               <div class="content">
                  <?php echo $step_1_desc; ?> </div>
               <!--/.content-->
            </div>
            <!--/.col-3-->
            <div class="process-step"> <img src="<?php echo $step_2_img; ?>">
               <h2 class="head-block"><?php echo $step_2_title; ?></h2>
               <div class="content">
                  <?php echo $step_2_desc; ?> </div>
               <!--/.content-->
            </div>
            <!--/.col-3-->
            <div class="process-step"> <img src="<?php echo $step_3_img; ?>">
               <h2 class="head-block"><?php echo $step_3_title; ?></h2>
               <div class="content">
                  <?php echo $step_3_desc; ?> </div>
               <!--/.content-->
            </div>
            <!--/.col-3-->
            <div class="process-step"> <img src="<?php echo $step_4_img; ?>">
               <h2 class="head-block"><?php echo $step_4_title; ?></h2>
               <div class="content">
                  <?php echo $step_4_desc; ?> </div>
               <!--/.content-->
            </div>
            <!--/.col-3-->
         </div>
         <!--/.row-->
   </section>
   <!--/.process-->
   <section class="bgDark our-clients">
      <div class="row">
         <h1 class="head-srf b cCoral cntr">W<i class="krn-8-l">e</i> <i class="hrt">♥</i> our clients</h1> </div>
      <?php
	$args=array(
		'post_type' => 'clients',
		'posts_per_page'=> 6,
		'meta_key' => 'has_detail_page',
		'meta_value' => 'yes',
		'orderby'   => 'menu_order',
	    'order'     => 'ASC'
	);

	$clients = null;
	$clients = new WP_Query($args);
		
	if($clients->have_posts()) {
	
?>
         <div class="featured-clients vanilla">
            <h2 class="head-block">Client Case Studies</h2>
            <ul class="list-4">
               <?php		
		$i = 1;
		while ($clients->have_posts()) : $clients->the_post();   
?>
                  <li>
                     <a href="<?php the_permalink(); ?>">
                        <?php the_title(); ?>&nbsp;<i class="arw"></i></a>
                  </li>
                  <?php				
			if($i % 2 == 0 || $i == $clients->post_count){
?>
            </ul>
            <?php				
				if($i != $clients->post_count){
?>
               <ul class="list-4">
                  <?php						
				}	
			} 
						
			$i++;
			
		endwhile;
?>
         </div>
         <?php		
	}
?>
            <?php
	$args=array(
		'post_type' => 'clients',
		'posts_per_page'=> 999,
		'orderby'   => 'title',
	    'order'     => 'ASC'
	);

	$clientsFull = null;
	$clientsFull = new WP_Query($args);
	
	if($clientsFull->have_posts()){
?>
               <div class="full-client-list" id="client-list">
                  <h2 class="head-block">Select Client List</h2>
                  <?php	
	
		$count 	= (int)$clientsFull->post_count;
		$perCol	= ceil($count / 4);

?>
                     <ul class="list-3 list-geo">
                        <?php		
		$i = 1;
		while ($clientsFull->have_posts()) : $clientsFull->the_post();  
?>
                           <li>
                              <?php the_title(); ?>
                           </li>
                           <?php			
			if($i % $perCol == 0 || $i == $count){
?>
                     </ul>
                     <?php
				if($i != $count){
?>
                        <ul class="list-3 list-geo">
                           <?php						
				}				
			}
		
		
			$i++;
		endwhile;
?>
               </div>
               <!--/.full-client-list-->
               <?php	
	
	}
	
?>
   </section>
   <!--/.bgDark-->
   <?php endwhile; ?>
   <?php endif; ?>
   <?php get_footer(); ?>