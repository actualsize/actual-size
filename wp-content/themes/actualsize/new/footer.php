<footer class="pageFooter test">
   <div class="contact vcard">
      <div class="contact-area start-project">
         <a href="/contact/" class="button-start-project">
            <h2 class="head-block">Can We Work Together?</h2>
            <div class="button-block">Fill Out This Exciting Form!&nbsp;<i class="arw"></i></div>
         </a>
      </div>
      <nav role="navigation" id="footer-nav">
         <?php  wp_nav_menu(array( 'menu' => 'main-navigation', 'menu_class'=>'main-navigation group' )); ?> </nav>
      <div class="contact-area social">
         <h2 class="head-block">Follow us</h2>
         <ul class="social-links group">
            <li><a href="https://www.facebook.com/ActualSizeCreative" target="_blank" class="facebook"><span class="icn">F</span><span class="visuallyhidden">acebook</span></a></li>
            <li><a href="https://twitter.com/ActualSizers" target="_blank" class="twitter"><span class="icn">T</span><span class="visuallyhidden">witter</span></a></li>
         </ul>
      </div>
      <div class="contact-area phone-email">
         <h2 class="head-block">Contact Us</h2>
         <div class="cpy-lrg"> <span class="tel">412 363 2100</span><br>
            <script type="text/javascript">
               //<![CDATA[
               <!--
               var x = "function f(x){var i,o=\"\",ol=x.length,l=ol;while(x.charCodeAt(l/13)!" + "=33){try{x+=x;l+=l;}catch(e){}}for(i=l-1;i>=0;i--){o+=x.charAt(i);}return o" + ".substr(0,ol);}f(\")79,\\\"320\\\\720\\\\530\\\\410\\\\`771\\\\!020\\\\200\\" + "\\PSX420\\\\\\\\\\\\B^eYUFFRqoAKBb420\\\\310\\\\tKODIF000\\\\}530\\\\lm|px:" + ";Dzyv:vhxccoxxhkIgahl>lvmi730\\\\020\\\\^'G730\\\\530\\\\500\\\\630\\\\U520" + "\\\\OPY630\\\\300\\\\310\\\\130\\\\500\\\\130\\\\530\\\\G430\\\\t\\\\300\\\\" + "010\\\\120\\\\000\\\\r\\\\500\\\\\\\"(f};o nruter};))++y(^)i(tAedoCrahc.x(e" + "doCrahCmorf.gnirtS=+o;721=%y;i=+y)79==i(fi{)++i;l<i;0=i(rof;htgnel.x=l,\\\"" + "\\\"=o,i rav{)y,x(f noitcnuf\")";
               while (x = eval(x));
               //-->
               //]]>
            </script>
         </div>
         <!--/.cpy-lrg-->
      </div>
      <div class="contact-area visit adr">
         <h2 class="head-block">Visit Us</h2>
         <div class="cpy-lrg">
            <h1 class="fn org head-visit">Actual Size</h1> <span class="street-address">5746 Baum Blvd.</span><br> <span class="locality">Pittsburgh</span>, <span class="region">PA</span> <span class="postal-code">15206</span> </div>
         <!--/.cpy-lrg--><a href="https://goo.gl/maps/cO3Jf" class="button-block" target="_blank">Get Directions&nbsp;<i class="arw"></i></a> </div>
   </div>
   <!--/.contact-->
</footer>
<!--/.pageFooter-->
<?php wp_footer(); ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/wp-content/themes/actual-size/_/js/plugins.js"></script>
<script>
   $(function()
   {
      FastClick.attach(document.body);
      $('#button-menu').click(function(e)
      {
         e.preventDefault();
         $('#nav').toggleClass('show');
         $(this).toggleClass('open');
      });
      $('.title-wrap').waypoint('sticky');
      $('img.lazy').lazyload(
      {
         threshold: 400,
         effect: "fadeIn"
      });
      $('.team-nav').find('a').smoothScroll();
      $('.vidPlayer').mediaelementplayer(
      {});
      var timeLapsePlayer = new MediaElementPlayer('#timelapse',
      {
         success: function(media)
         {
            media.addEventListener('play', function(media)
            {
               $('.what-we-do-overlay').removeClass('default');
            }, true)
         }
      });
      $('.what-we-do-overlay').click(function()
      {
         timeLapsePlayer.play();
      });
      $('.head-what-we-do .line-1').fitText(3,
      {
         minFontSize: '18px',
         maxFontSize: '30px'
      });
      $('.head-what-we-do .line-2').fitText(1.35,
      {
         minFontSize: '40px',
         maxFontSize: '90px'
      });
      $('.single-projects').keydown(function(e)
      {
         if (e.keyCode == 37)
         {
            $("#prev-project")[0].click();
            e.preventDefault();
         }
         else if (e.keyCode == 39)
         {
            $("#next-project")[0].click();
            e.preventDefault();
         }
      });
   });
</script>
<script>
   document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')
</script>
<script>
   (function(i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function()
      {
         (i[r].q = i[r].q || []).push(arguments)
      }, i[r].l = 1 * new Date();
      a = s.createElement(o),
         m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
   })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
   ga('create', 'UA-40002630-1', 'actualsize.com');
   ga('send', 'pageview');
</script>
</body>

</html>