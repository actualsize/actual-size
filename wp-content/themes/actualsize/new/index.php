<?php get_header(); ?>
<div class="blog-outer">
   <?php

if(is_home() && !is_paged()) { 	
	$args=array(
		'post_type' => 'post',
		'posts_per_page'=> 1,
		'orderby'   => 'date',
		'order'     => 'DESC'
	);
	
	$main_post = null;
	$main_post = new WP_Query($args);
	
	if($main_post->have_posts()) {
		while ($main_post->have_posts()) : $main_post->the_post();
			$main_post_id = $post->ID;
?>
      <article <?php post_class() ?> id="post-
         <?php the_ID(); ?>">
         <div class="post-detail landing">
            <div class="post-head-landing">
               <?php 
						$image = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'large');

						if($image){
?> <a href="<?php the_permalink(); ?>"><img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>"></a>
               <?php														
						} 
?>
                  <h1 class="head-block e"><a href="<?php the_permalink(); ?>" class="cCoral"><?php the_title(); ?></a></h1> </div>
            <div class="inner">
               <a href="<?php the_permalink(); ?>">
                  <div class="post-content">
                     <?php the_excerpt(); ?> </div>
                  <!--/.post-content-->
               </a>
               <div class="read-more"> <a href="<?php the_permalink(); ?>" class="button-block hollow">Read More <i class="arw"></i></a> </div>
            </div>
            <!--/.inner-->
            <?php
					$author_id = $post->post_author;
					$author_headshot	= get_field('user_headshot', 'user_'.$author_id);
					$author_color 		= get_field('user_color', 'user_'.$author_id);
					$author_anchor 		= get_field('user_anchor', 'user_'.$author_id);
					if($author_headshot){
						$author_headshot = $author_headshot['sizes']['team-headshot'];
					}
?>
               <a href="<?php echo get_permalink(168); echo '#'.$author_anchor; ?>" class="post-author-sm">
                  <?php 
						
						
?> From The Desk Of
                  <?php if($author_headshot){ ?>
                  <div class="author-img sm"><img src="<?php echo $author_headshot; ?>"></div>
                  <?php } ?><span class="author-name" style="color: <?php echo $author_color; ?>;"><?php echo get_the_author(); ?></span> </a>
               <!--/.post-author-->
         </div>
         <!--/.post-detail-->
      </article>
      <?php
		
		endwhile;
		
	}
	
}
?>
         <aside class="more-posts <?php if(is_paged()) { echo 'paged'; } ?>">
            <div class="inner">
               <?php
		if(is_home() && !is_paged() && have_posts() && (count($posts) > 1)){  
?>
                  <h1 class="head-srf a cntr lined shallow"><span class="txt single">More Stuff W<i class="krn-6-l">e</i> W<i class="krn-6-l">r</i>ote <span class="lrg-only">for You</span></span></h1>
                  <?php
		}
?>
                     <div class="list-posts">
                        <?php 
	
		if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php
			if($main_post_id != $post->ID){
				$author_id 			= $post->post_author;
				$author_color 		= get_field('user_color', 'user_'.$author_id);
				$author_anchor 		= get_field('user_anchor', 'user_'.$author_id);
				$post_thumb 		  = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'team-headshot');				
				
?>
                           <article <?php post_class( 'post-tease') ?> id="post-
                              <?php the_ID(); ?>">
                              <div class="img">
                                 <?php
					if($post_thumb){
?> <a href="<?php the_permalink(); ?>"><img src="<?php echo $post_thumb[0]; ?>"></a>
                                    <?php				
					} else {
?> &nbsp;
                                       <?php				
					}
?>
                              </div>
                              <!_-/.img-->
                              <div class="detail">
                                 <h1 class="head-thin"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                                 <a href="<?php the_permalink(); ?>">
                                    <div class="content">
                                       <?php the_excerpt(); ?> </div>
                                    <!--/.content-->
                                 </a> <a href="<?php echo get_permalink(168); echo '#'.$author_anchor; ?>" class="tease-author">From The Desk Of <span class="author-name" style="color: <?php echo $author_color; ?>;"><?php echo get_the_author(); ?></span></a>                                 </div>
                              <!--/.detail-->
                           </article>
                           <?php
			}
?>
                              <?php endwhile; ?>
                              <?php include (TEMPLATEPATH . '/_/inc/nav.php' ); ?>
                              <?php else : ?>
                              <div class="row low post-detail clipped">
                                 <div class="col-10">
                                    <h2 class="head-geo">Page Not Found</h2>
                                    <div class="content-generic xl">
                                       <p>Sorry, the page you were looking for could not be found, or may never have existed.</p>
                                       <p>If you’d like, you can try <a href="#searchform" class="scroll" style="text-decoration: underline;">searching</a> for it.</p>
                                       <p>&nbsp;</p>
                                       <p>&nbsp;</p>
                                    </div>
                                    <!--/.content-generic-->
                                 </div>
                              </div>
                              <?php endif; ?> </div>
                     <!--/.list-posts-->
            </div>
            <!--/.inner-->
         </aside>
         <!--/.more-posts-->
</div>
<!--/.blog-outer-->
<?php get_sidebar(); ?>
<?php get_footer(); ?>