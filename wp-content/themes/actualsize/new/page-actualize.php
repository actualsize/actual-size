<?php

/* 
Template Name: Actualize
*/

?>
   <?php get_header(); ?>
   <div class="bgGrey">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <section class="work-with-us">
         <h1 class="head-srf a lined shallow"><i class="txt">Actualize</i></h1>
         <h2>Instructions</h2>
         <p>We’ve put together this form to help us capture your vision for your website project. Think of this as a business plan for your website. Answer as many questions as they pertain to your project and skip the ones you feel do not.</p>
         <p>Because each member of your team may have a different vision for the project, we recommend completing this worksheet separately. That way we won’t miss any important opinions. Once you’re satisfied that the worksheet clearly describes your project,
            click submit and share your answers with us. <strong>Your privacy is important to us. Everything you tell us is strictly confidential.</strong></p>
         <?php the_content(); ?> </section>
      <!--/.work-with-us-->
      <?php endwhile; ?>
      <?php endif; ?> </div>
   <!--/.bgGrey-->
   <?php get_footer(); ?>