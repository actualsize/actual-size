<?php

    // Remove WordPress version number for security ----------------------------------
        function complete_version_removal() {
            return '';
        }
        add_filter('the_generator', 'complete_version_removal');


    // Set permalink structure -------------------------------------------------------
        // add_action('init', function() {
        //     global $wp_rewrite;
        //     $wp_rewrite->set_permalink_structure('%postname%/');
        // });


    // Add Support for post thumbnails -----------------------------------------------
        add_theme_support( 'post-thumbnails' );


    // Add new size for post thumbnails ----------------------------------------------
        add_image_size('more-thumb', 60, 60, true);
        add_image_size('more-thumb-2x', 120, 120, true);
        add_image_size('mobile', 600, 600, true);


    // Remove trailing slash ---------------------------------------------------------
        function permalink_untrailingslashit($link) {
            return untrailingslashit($link);
        }
        add_filter('page_link', 'permalink_untrailingslashit');
        add_filter('post_type_link', 'permalink_untrailingslashit');


    // Remove post boxes -------------------------------------------------------------
        function remove_editor() {
          remove_post_type_support('post', 'editor');
          remove_post_type_support('post', 'comments');
          remove_post_type_support('post', 'author');
          remove_post_type_support('post', 'categories');
          remove_post_type_support('post', 'trackbacks');
          remove_post_type_support('post', 'revisions');
          remove_post_type_support('post', 'custom-fields');
          remove_post_type_support('post', 'page-attributes');
          remove_post_type_support('post', 'post-formats');

        }
        add_action('admin_init', 'remove_editor');


    // Add custom meta box for saints ------------------------------------------------
        function information_add_meta_box() {
            add_meta_box(
                'information-information',
                __( 'Information', 'information' ),
                'information_information_html',
                'post',
                'normal',
                'default'
            );
        }
        add_action( 'add_meta_boxes', 'information_add_meta_box' );


    // Add custom meta box for publishers page only ----------------------------------
        function publishers_add_meta_box() {
            add_meta_box(
                'publishers-publishers',
                __( 'Publishers', 'publishers' ),
                'publishers_publishers_html',
                'page',
                'normal',
                'default'
            );
        }

        if (! empty($_GET['post'])) {
            add_action( 'admin_init' , 'my_meta_init' );
        }

        function my_meta_init() {
            $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];

            if ($post_id == '57' || $post_id == '76') {
                add_action( 'add_meta_boxes', 'publishers_add_meta_box' );
            }
            if ($post_id == '155') {
                add_action( 'add_meta_boxes', 'testimonials_add_meta_box' );
            }
        }


    // Set up list of publishers -----------------------------------------------------
        function publishers_publishers_html( $post ) {
            wp_nonce_field( '_publishers_publishers_nonce', 'publishers_publishers_nonce' );
            ?>

            <style>
                .wp-publishers-information-box {
                    border-bottom: thin solid gainsboro;
                    padding: 15px 0;
                }
                .wp-publishers-information-box:last-child { border: 0; }
                .wp-publisher-fieldset { padding: 1px 0;     }
                .wp-publishers-input {
                    display: inline-block;
                    width: 40%;
                }
                .wp-publishers-inline-label {
                    display: inline-block;
                    width: 5.5em;
                }
                .wp-publishers-type {
                    display: inline;
                }
            </style>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_1"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_1" id="publishers_publishers_name_item_1" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_1' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_1"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_1" id="publishers_publishers_type_item_1" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_1' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_1"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_1" id="publishers_publishers_location_item_1" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_1' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_2"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_2" id="publishers_publishers_name_item_2" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_2' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_2"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_2" id="publishers_publishers_type_item_2" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_2' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_2"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_2" id="publishers_publishers_location_item_2" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_2' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_3"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_3" id="publishers_publishers_name_item_3" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_3' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_3"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_3" id="publishers_publishers_type_item_3" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_3' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_3"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_3" id="publishers_publishers_location_item_3" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_3' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_4"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_4" id="publishers_publishers_name_item_4" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_4' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_4"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_4" id="publishers_publishers_type_item_4" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_4' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_4"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_4" id="publishers_publishers_location_item_4" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_4' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_5"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_5" id="publishers_publishers_name_item_5" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_5' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_5"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_5" id="publishers_publishers_type_item_5" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_5' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_5"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_5" id="publishers_publishers_location_item_5" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_5' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_6"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_6" id="publishers_publishers_name_item_6" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_6' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_6"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_6" id="publishers_publishers_type_item_6" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_6' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_6"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_6" id="publishers_publishers_location_item_6" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_6' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_7"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_7" id="publishers_publishers_name_item_7" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_7' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_7"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_7" id="publishers_publishers_type_item_7" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_7' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_7"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_7" id="publishers_publishers_location_item_7" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_7' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_8"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_8" id="publishers_publishers_name_item_8" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_8' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_8"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_8" id="publishers_publishers_type_item_8" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_8' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_8"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_8" id="publishers_publishers_location_item_8" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_8' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_9"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_9" id="publishers_publishers_name_item_9" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_9' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_9"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_9" id="publishers_publishers_type_item_9" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_9' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_9"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_9" id="publishers_publishers_location_item_9" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_9' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_10"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_10" id="publishers_publishers_name_item_10" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_10' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_10"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_10" id="publishers_publishers_type_item_10" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_10' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_10"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_10" id="publishers_publishers_location_item_10" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_10' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_11"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_11" id="publishers_publishers_name_item_11" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_11' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_11"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_11" id="publishers_publishers_type_item_11" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_11' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_11"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_11" id="publishers_publishers_location_item_11" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_11' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_12"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_12" id="publishers_publishers_name_item_12" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_12' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_12"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_12" id="publishers_publishers_type_item_12" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_12' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_12"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_12" id="publishers_publishers_location_item_12" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_12' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_13"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_13" id="publishers_publishers_name_item_13" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_13' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_13"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_13" id="publishers_publishers_type_item_13" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_13' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_13"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_13" id="publishers_publishers_location_item_13" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_13' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_14"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_14" id="publishers_publishers_name_item_14" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_14' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_14"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_14" id="publishers_publishers_type_item_14" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_14' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_14"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_14" id="publishers_publishers_location_item_14" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_14' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_15"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_15" id="publishers_publishers_name_item_15" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_15' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_15"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_15" id="publishers_publishers_type_item_15" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_15' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_15"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_15" id="publishers_publishers_location_item_15" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_15' ); ?>">
                </div>
            </div>

            <div class="wp-publishers-information-box">
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_name_item_16"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_name_item_16" id="publishers_publishers_name_item_16" value="<?php echo publishers_get_meta( 'publishers_publishers_name_item_16' ); ?>">
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_type_item_16"><?php _e( 'Type', 'information' ); ?></label>
                    <input list="publishers-type" class="wp-publishers-input" type="text" name="publishers_publishers_type_item_16" id="publishers_publishers_type_item_16" value="<?php echo publishers_get_meta( 'publishers_publishers_type_item_16' ); ?>">
                        <datalist id="publishers-type">
                            <option value="Newspaper">
                            <option value="Bulletin">
                            <option value="Magazine">
                        </datalist>
                </div>
                <div class="wp-publisher-fieldset">
                    <label class="wp-publishers-inline-label" for="publishers_publishers_location_item_16"><?php _e( 'Location', 'information' ); ?></label>
                    <input class="wp-publishers-input" type="text" name="publishers_publishers_location_item_16" id="publishers_publishers_location_item_16" value="<?php echo publishers_get_meta( 'publishers_publishers_location_item_16' ); ?>">
                </div>
            </div>
        <? }


    // Get custom meta info ------------------------------------------------------
        function information_get_meta( $value ) {
            global $post;

            $field = get_post_meta( $post->ID, $value, true );
            if ( ! empty( $field ) ) {
                return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
            } else {
                return false;
            }
        }

        function publishers_get_meta( $value ) {
            global $post;

            $field = get_post_meta( $post->ID, $value, true );
            if ( ! empty( $field ) ) {
                return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
            } else {
                return false;
            }
        }

        function testimonials_get_meta( $value ) {
            global $post;

            $field = get_post_meta( $post->ID, $value, true );
            if ( ! empty( $field ) ) {
                return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
            } else {
                return false;
            }
        }


    // Set up custom meta box info ---------------------------------------------------
        function information_information_html( $post ) {
            wp_nonce_field( '_information_information_nonce', 'information_information_nonce' ); ?>

            <style>
                .wp-saint-info-box input, .wp-saint-info-box textarea {
                    width: 100%;
                    max-width: 100%;
                    max-height: 100px;
                }
                .wp-saint-prefix-example {
                    font-size: smaller;
                    padding: 0 2px;
                }
                .wp-saint-quote { width: 100%; min-height: 4em; }

                .wp-saint-prefix, .wp-saint-quote-prefix { width: 140px; }
                .wp-feast-month-input { width: 140px; }
                    input::-webkit-calendar-picker-indicator { display: none; }
                .wp-feast-date-input { width: 38px; }
                .wp-saint-background-picker {
                    display: -webkit-flex;
                    display: -moz-flex;
                    display: flex;
                    -webkit-align-items: center;
                    -moz-align-items: center;
                    align-items: center;
                }
                .wp-saint-background-picker input { display: none; }
                .wp-saint-background-picker label {
                    display: inline-block;
                    width: 2.25em;
                    height: 2.25em;
                    margin-left: .5em;
                    border-radius: 50%;
                    border: .25em solid transparent;
                }
                .wp-saint-background-picker label:first-of-type { margin-left: 2em; }
                .wp-saint-background-picker input:checked + label {
                    border-color: #5B9DD9;
                    box-shadow: inset 0 0 0 .25em #fff, 0 0 2px rgba(30, 140, 190, 0.8);
                }
            </style>
            <p>
                <label for="information_information_saint_prefix"><?php _e( 'Prefix', 'information' ); ?> (<code class="wp-saint-prefix-example">St.</code>, <code class="wp-saint-prefix-example">Bl.</code>, <code class="wp-saint-prefix-example">Ven.</code>, <code class="wp-saint-prefix-example">Pope St.</code>, <code class="wp-saint-prefix-example">Servant of God</code>)</label><br>
                <input class="wp-saint-prefix" type="text" name="information_information_saint_prefix" id="information_information_saint_prefix" value="<?php echo information_get_meta( 'information_information_saint_prefix' ); ?>">
            </p>
            <p>
                <label for="information_information_feast_month"><?php _e( 'Feast Day', 'information' ); ?> (<?php echo date('F j'); ?>)</label><br>
                <input list="months" class="wp-feast-month-input" type="text" name="information_information_feast_month" id="information_information_feast_month" value="<?php echo information_get_meta( 'information_information_feast_month' ); ?>">
                <datalist id="months">
                    <option value="January">
                    <option value="February">
                    <option value="March">
                    <option value="April">
                    <option value="May">
                    <option value="June">
                    <option value="July">
                    <option value="August">
                    <option value="September">
                    <option value="October">
                    <option value="November">
                    <option value="December">
                </datalist>
                <input class="wp-feast-date-input" type="text" maxlength="2" name="information_information_feast_date" id="information_information_feast_date" value="<?php echo information_get_meta( 'information_information_feast_date' ); ?>">
            </p>
            <p class="wp-saint-info-box">
                <label for="information_information_1st_fact"><?php _e( '1st Fact', 'information' ); ?></label><br>
                <input type="text" name="information_information_1st_fact" id="information_information_1st_fact" value="<?php echo information_get_meta( 'information_information_1st_fact' ); ?>">
            </p>
            <p class="wp-saint-info-box">
                <label for="information_information_2nd_fact"><?php _e( '2nd Fact', 'information' ); ?></label><br>
                <input type="text" name="information_information_2nd_fact" id="information_information_2nd_fact" value="<?php echo information_get_meta( 'information_information_2nd_fact' ); ?>">
            </p>
            <p class="wp-saint-info-box">
                <label for="information_information_3rd_fact"><?php _e( '3rd Fact', 'information' ); ?></label><br>
                <input type="text" name="information_information_3rd_fact" id="information_information_3rd_fact" value="<?php echo information_get_meta( 'information_information_3rd_fact' ); ?>">
            </p>
            <p>
                <label for="information_information_quote"><?php _e( 'Quote', 'information' ); ?></label><br>
                <textarea class="wp-saint-quote" name="information_information_quote" id="information_information_quote" ><?php echo information_get_meta( 'information_information_quote' ); ?></textarea>

                <label for="information_information_quote_citation"><?php _e( 'Quote citation', 'information' ); ?></label>
                <input class="wp-saint-quote-prefix" type="text" name="information_information_quote_citation" id="information_information_quote_citation" value="<?php echo information_get_meta( 'information_information_quote_citation' ); ?>">
                <label for="information_information_quote_citation"><em>(Optional)</em></label>
            </p>
            <p class="wp-saint-info-box wp-saint-background-picker">
                Background:
                <input type="radio" name="information_information_background" id="information_information_background_0" value="red" <?php echo ( information_get_meta( 'information_information_background' ) === 'red' ) ? 'checked' : ''; ?>>
                <label style="background-color: #C6796F" for="information_information_background_0"></label>
                <input type="radio" name="information_information_background" id="information_information_background_1" value="orange" <?php echo ( information_get_meta( 'information_information_background' ) === 'orange' ) ? 'checked' : ''; ?>>
                <label style="background-color: #CD9D6A" for="information_information_background_1"></label>
                <input type="radio" name="information_information_background" id="information_information_background_2" value="yellow" <?php echo ( information_get_meta( 'information_information_background' ) === 'yellow' ) ? 'checked' : ''; ?>>
                <label style="background-color: #C8B26A" for="information_information_background_2"></label>
                <input type="radio" name="information_information_background" id="information_information_background_3" value="green" <?php echo ( information_get_meta( 'information_information_background' ) === 'green' ) ? 'checked' : ''; ?>>
                <label style="background-color: #8DC772" for="information_information_background_3"></label>
                <input type="radio" name="information_information_background" id="information_information_background_4" value="teal" <?php echo ( information_get_meta( 'information_information_background' ) === 'teal' ) ? 'checked' : ''; ?>>
                <label style="background-color: #77C099" for="information_information_background_4"></label>
                <input type="radio" name="information_information_background" id="information_information_background_5" value="blue" <?php echo ( information_get_meta( 'information_information_background' ) === 'blue' ) ? 'checked' : ''; ?>>
                <label style="background-color: #5FA3B6" for="information_information_background_5"></label>
                <input type="radio" name="information_information_background" id="information_information_background_6" value="purple" <?php echo ( information_get_meta( 'information_information_background' ) === 'purple' ) ? 'checked' : ''; ?>>
                <label style="background-color: #AF92CD" for="information_information_background_6"></label>
                <input type="radio" name="information_information_background" id="information_information_background_7" value="pink" <?php echo ( information_get_meta( 'information_information_background' ) === 'pink' ) ? 'checked' : ''; ?>>
                <label style="background-color: #CF99BE" for="information_information_background_7"></label>
            </p>
        <?php }


    // Remove admin pages ------------------------------------------------------------
        function sb_remove_admin_menus () {
          if (function_exists('remove_menu_page')) {
            // remove_menu_page('edit.php?post_type=page');
            // remove_menu_page('upload.php');
            remove_menu_page('link-manager.php');
            remove_menu_page('edit-comments.php');
            remove_menu_page('themes.php');
            // remove_menu_page('tools.php');
            // remove_menu_page('plugins.php');
            // remove_submenu_page('options-general.php', 'options-writing.php');
            remove_submenu_page('options-general.php', 'options-discussion.php');
            // remove_submenu_page('options-general.php', 'options-media.php');
            remove_submenu_page('options-general.php', 'options-reading.php');
          }
        }
        add_action('admin_menu', 'sb_remove_admin_menus');


    // Rename posts admin tab --------------------------------------------------------
        add_filter( 'gettext',  'change_post_to_portfolio' );
        add_filter( 'ngettext',  'change_post_to_portfolio' );
        function change_post_to_portfolio( $translated ) {
          $translated = str_ireplace(  'Posts',  'Saints',  $translated );  // ireplace is PHP5 only
          return $translated;
        }

        add_filter( 'gettext',  'change_pages_to_portfolio' );
        add_filter( 'ngettext',  'change_pages_to_portfolio' );
        function change_pages_to_portfolio( $translated ) {
          $translated = str_ireplace(  'Pages',  'Sections',  $translated );  // ireplace is PHP5 only
          return $translated;
        }


    // Add social media page input URLs ----------------------------------------------
        $new_general_setting = new new_general_setting();
        class new_general_setting {
            function new_general_setting( ) {
                add_filter('admin_init' , array( &$this , 'register_twitter_fields') );
                add_filter('admin_init' , array( &$this , 'register_facebook_fields') );
                add_filter('admin_init' , array( &$this , 'register_instagram_fields') );
            }
            function register_twitter_fields() {
                register_setting( 'general', 'twitter_url', 'esc_attr' );
                add_settings_field('twitter_url', '<label for="twitter_url">'.__('Twitter page URL' , 'twitter_url' ).'</label>' , array(&$this, 'twitter_fields_html') , 'general' );
            }
            function register_facebook_fields() {
                register_setting( 'general', 'facebook_url', 'esc_attr' );
                add_settings_field('facebook_url', '<label for="facebook_url">'.__('Facebook page URL' , 'facebook_url' ).'</label>' , array(&$this, 'facebook_fields_html') , 'general' );
            }
            function register_instagram_fields() {
                register_setting( 'general', 'instagram_url', 'esc_attr' );
                add_settings_field('instagram_url', '<label for="instagram_url">'.__('Instagram page URL' , 'instagram_url' ).'</label>' , array(&$this, 'facebook_fields_html') , 'general' );
            }
            function twitter_fields_html() {
                $twitter_value = get_option('twitter_url', '');
                echo '<input class="regular-text code" type="url" id="twitter_url" name="twitter_url" value="' . $twitter_value . '" />';
            }
            function facebook_fields_html() {
                $facebook_value = get_option('facebook_url', '');
                echo '<input class="regular-text code" type="url" id="facebook_url" name="facebook_url" value="' . $facebook_value . '" />';
            }
            function instagram_fields_html() {
                $instagram_value = get_option('instagram_url', '');
                echo '<input class="regular-text code" type="url" id="instagram_url" name="instagram_url" value="' . $instagram_value . '" />';
            }
        }


    // Save custom meta box info ---------------------------------------------
        function information_information_save( $post_id ) {
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
            if ( ! isset( $_POST['information_information_nonce'] ) || ! wp_verify_nonce( $_POST['information_information_nonce'], '_information_information_nonce' ) ) return;
            if ( ! current_user_can( 'edit_post' ) ) return;

            if ( isset( $_POST['information_information_saint_prefix'] ) )
                update_post_meta( $post_id, 'information_information_saint_prefix', esc_attr( $_POST['information_information_saint_prefix'] ) );
            if ( isset( $_POST['information_information_feast_month'] ) )
                update_post_meta( $post_id, 'information_information_feast_month', esc_attr( $_POST['information_information_feast_month'] ) );
            if ( isset( $_POST['information_information_feast_date'] ) )
                update_post_meta( $post_id, 'information_information_feast_date', esc_attr( $_POST['information_information_feast_date'] ) );
            if ( isset( $_POST['information_information_feast_month'] ) && isset( $_POST['information_information_feast_date'] ) ) {
                $month = esc_attr( $_POST['information_information_feast_month'] );
                $date = intval( esc_attr( $_POST['information_information_feast_date'] ) );
                if ($month == "January") { $month_days = 0; }
                if ($month == "February") { $month_days = 31; }
                if ($month == "March") { $month_days = 59; }
                if ($month == "April") { $month_days = 90; }
                if ($month == "May") { $month_days = 120; }
                if ($month == "June") { $month_days = 151; }
                if ($month == "July") { $month_days = 181; }
                if ($month == "August") { $month_days = 212; }
                if ($month == "September") { $month_days = 243; }
                if ($month == "October") { $month_days = 273; }
                if ($month == "November") { $month_days = 304; }
                if ($month == "December") { $month_days = 334; }

                $digit = $month_days + $date;

                update_post_meta( $post_id, 'information_information_feast_digit', esc_attr( $digit ) );
            }
            if ( isset( $_POST['information_information_1st_fact'] ) )
                update_post_meta( $post_id, 'information_information_1st_fact', esc_attr( $_POST['information_information_1st_fact'] ) );
            if ( isset( $_POST['information_information_2nd_fact'] ) )
                update_post_meta( $post_id, 'information_information_2nd_fact', esc_attr( $_POST['information_information_2nd_fact'] ) );
            if ( isset( $_POST['information_information_3rd_fact'] ) )
                update_post_meta( $post_id, 'information_information_3rd_fact', esc_attr( $_POST['information_information_3rd_fact'] ) );
            if ( isset( $_POST['information_information_quote'] ) )
                update_post_meta( $post_id, 'information_information_quote', esc_attr( $_POST['information_information_quote'] ) );
            if ( isset( $_POST['information_information_quote_citation'] ) )
                update_post_meta( $post_id, 'information_information_quote_citation', esc_attr( $_POST['information_information_quote_citation'] ) );
            if ( isset( $_POST['information_information_background'] ) )
                update_post_meta( $post_id, 'information_information_background', esc_attr( $_POST['information_information_background'] ) );
        }
        add_action( 'save_post', 'information_information_save' );

        function publishers_publishers_save( $post_id ) {
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
            if ( ! isset( $_POST['publishers_publishers_nonce'] ) || ! wp_verify_nonce( $_POST['publishers_publishers_nonce'], '_publishers_publishers_nonce' ) ) return;
            // if ( ! current_user_can( 'edit_post' ) ) return;

            if ( isset( $_POST['publishers_publishers_name_item_1'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_1', esc_attr( $_POST['publishers_publishers_name_item_1'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_1'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_1', esc_attr( $_POST['publishers_publishers_type_item_1'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_1'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_1', esc_attr( $_POST['publishers_publishers_location_item_1'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_2'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_2', esc_attr( $_POST['publishers_publishers_name_item_2'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_2'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_2', esc_attr( $_POST['publishers_publishers_type_item_2'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_2'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_2', esc_attr( $_POST['publishers_publishers_location_item_2'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_3'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_3', esc_attr( $_POST['publishers_publishers_name_item_3'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_3'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_3', esc_attr( $_POST['publishers_publishers_type_item_3'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_3'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_3', esc_attr( $_POST['publishers_publishers_location_item_3'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_4'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_4', esc_attr( $_POST['publishers_publishers_name_item_4'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_4'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_4', esc_attr( $_POST['publishers_publishers_type_item_4'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_4'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_4', esc_attr( $_POST['publishers_publishers_location_item_4'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_5'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_5', esc_attr( $_POST['publishers_publishers_name_item_5'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_5'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_5', esc_attr( $_POST['publishers_publishers_type_item_5'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_5'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_5', esc_attr( $_POST['publishers_publishers_location_item_5'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_6'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_6', esc_attr( $_POST['publishers_publishers_name_item_6'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_6'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_6', esc_attr( $_POST['publishers_publishers_type_item_6'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_6'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_6', esc_attr( $_POST['publishers_publishers_location_item_6'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_7'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_7', esc_attr( $_POST['publishers_publishers_name_item_7'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_7'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_7', esc_attr( $_POST['publishers_publishers_type_item_7'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_7'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_7', esc_attr( $_POST['publishers_publishers_location_item_7'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_8'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_8', esc_attr( $_POST['publishers_publishers_name_item_8'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_8'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_8', esc_attr( $_POST['publishers_publishers_type_item_8'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_8'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_8', esc_attr( $_POST['publishers_publishers_location_item_8'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_9'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_9', esc_attr( $_POST['publishers_publishers_name_item_9'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_9'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_9', esc_attr( $_POST['publishers_publishers_type_item_9'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_9'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_9', esc_attr( $_POST['publishers_publishers_location_item_9'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_10'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_10', esc_attr( $_POST['publishers_publishers_name_item_10'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_10'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_10', esc_attr( $_POST['publishers_publishers_type_item_10'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_10'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_10', esc_attr( $_POST['publishers_publishers_location_item_10'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_11'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_11', esc_attr( $_POST['publishers_publishers_name_item_11'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_11'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_11', esc_attr( $_POST['publishers_publishers_type_item_11'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_11'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_11', esc_attr( $_POST['publishers_publishers_location_item_11'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_12'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_12', esc_attr( $_POST['publishers_publishers_name_item_12'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_12'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_12', esc_attr( $_POST['publishers_publishers_type_item_12'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_12'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_12', esc_attr( $_POST['publishers_publishers_location_item_12'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_13'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_13', esc_attr( $_POST['publishers_publishers_name_item_13'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_13'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_13', esc_attr( $_POST['publishers_publishers_type_item_13'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_13'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_13', esc_attr( $_POST['publishers_publishers_location_item_13'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_14'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_14', esc_attr( $_POST['publishers_publishers_name_item_14'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_14'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_14', esc_attr( $_POST['publishers_publishers_type_item_14'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_14'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_14', esc_attr( $_POST['publishers_publishers_location_item_14'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_15'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_15', esc_attr( $_POST['publishers_publishers_name_item_15'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_15'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_15', esc_attr( $_POST['publishers_publishers_type_item_15'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_15'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_15', esc_attr( $_POST['publishers_publishers_location_item_15'] ) );

            if ( isset( $_POST['publishers_publishers_name_item_16'] ) )
                update_post_meta( $post_id, 'publishers_publishers_name_item_16', esc_attr( $_POST['publishers_publishers_name_item_16'] ) );
            if ( isset( $_POST['publishers_publishers_type_item_16'] ) )
                update_post_meta( $post_id, 'publishers_publishers_type_item_16', esc_attr( $_POST['publishers_publishers_type_item_16'] ) );
            if ( isset( $_POST['publishers_publishers_location_item_16'] ) )
                update_post_meta( $post_id, 'publishers_publishers_location_item_16', esc_attr( $_POST['publishers_publishers_location_item_16'] ) );
        }
        add_action( 'save_post', 'publishers_publishers_save' );


    // Upload and Save PDF ---------------------------------------------
        function update_edit_form() {
            echo 'enctype="multipart/form-data"';
        }
        add_action('post_edit_form_tag', 'update_edit_form');

        function add_custom_meta_boxes() {
            add_meta_box(
                'wp_custom_attachment',
                'Coloring Sheet',
                'wp_custom_attachment',
                'post',
                'side'
            );
        }
        add_action('add_meta_boxes', 'add_custom_meta_boxes');

        function wp_custom_attachment_get_meta( $value ) {
            global $post;

            $array = get_post_meta( $post->ID, $value, true )["url"];
            $field = substr($array, 0);

            if ( ! empty( $field ) ) {
                $html .= '<p class="wp-coloring-sheet-label">Current sheet:</p>';
                $html .= '<input class="wp-current-coloring-sheet" value="' . $field . '">';

                return $html;
            } else {
                return false;
            }
        }

        function wp_custom_attachment() {
            wp_nonce_field(plugin_basename(__FILE__), 'wp_custom_attachment_nonce');

            $html .= wp_custom_attachment_get_meta( 'wp_custom_attachment' );
            $html .= '<input type="file" id="wp_custom_attachment" name="wp_custom_attachment" value="" size="25" />';
            $html .= '<style>input[name=\'wp_custom_attachment\'] { font-size: larger; color: transparent; } .wp-coloring-sheet-label { margin-bottom: 2px; } .wp-current-coloring-sheet { margin-bottom: 10px; display: block; width: 100%; direction: rtl; }</style>';

            echo $html;
        }

        function save_custom_meta_data($id) {
            if(!wp_verify_nonce($_POST['wp_custom_attachment_nonce'], plugin_basename(__FILE__))) { return $id; }
            if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { return $id; }

            if('page' == $_POST['post_type']) {
              if(!current_user_can('edit_page', $id)) {
                return $id;
              }
            } else {
                if(!current_user_can('edit_page', $id)) {
                    return $id;
                }
            }

            if(!empty($_FILES['wp_custom_attachment']['name'])) {
                $supported_types = array('application/pdf');
                $arr_file_type = wp_check_filetype(basename($_FILES['wp_custom_attachment']['name']));
                $uploaded_type = $arr_file_type['type'];

                if(in_array($uploaded_type, $supported_types)) {
                    $upload = wp_upload_bits($_FILES['wp_custom_attachment']['name'], null, file_get_contents($_FILES['wp_custom_attachment']['tmp_name']));

                    if(isset($upload['error']) && $upload['error'] != 0) {
                        wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
                    } else {
                        add_post_meta($id, 'wp_custom_attachment', $upload);
                        update_post_meta($id, 'wp_custom_attachment', $upload);
                    }

                    unset( $_FILES['wp_custom_attachment'] );
                } else {
                    wp_die("The file type that you've uploaded is not a PDF.");
                }
            }
        }
        add_action('save_post', 'save_custom_meta_data');


    // Set up list of testimonials -----------------------------------------------------
        function testimonials_testimonials_html( $post ) {
            wp_nonce_field( '_testimonials_testimonials_nonce', 'testimonials_testimonials_nonce' );
            ?>

            <style>
                .wp-testimonials-information-box {
                    border-bottom: thin solid gainsboro;
                    padding: 15px 0;
                }
                .wp-testimonials-information-box:last-child { border: 0; }
                .wp-testimonial-fieldset { padding: 1px 0; }
                .wp-testimonials-textarea {
                    display: inline-block;
                    width: calc(100% - 5.5em);
                }
                .wp-testimonials-input {
                    display: inline-block;
                    width: 40%;
                }
                .wp-testimonials-inline-label {
                    display: inline-block;
                    width: 5.5em;
                }
                .wp-testimonials-inline-label:first-child {
                    vertical-align: top;
                }
                .wp-testimonials-type {
                    display: inline;
                }
            </style>

            <div class="wp-testimonials-information-box">
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_1">Quote</label>
                    <textarea class="wp-testimonials-textarea" name="testimonials_testimonials_quote_item_1"><?php echo testimonials_get_meta( 'testimonials_testimonials_quote_item_1' ); ?></textarea>
                </div>
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_1"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-testimonials-input" type="text" name="testimonials_testimonials_name_item_1" id="testimonials_testimonials_name_item_1" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_item_1' ); ?>">
                    <input class="wp-testimonials-input" placeholder="Title (Optional)" type="text" name="testimonials_testimonials_name_title_1" id="testimonials_testimonials_name_title_item_1" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_title_1' ); ?>">
                </div>
            </div>

            <div class="wp-testimonials-information-box">
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_2">Quote</label>
                    <textarea class="wp-testimonials-textarea" name="testimonials_testimonials_quote_item_2"><?php echo testimonials_get_meta( 'testimonials_testimonials_quote_item_2' ); ?></textarea>
                </div>
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_2"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-testimonials-input" type="text" name="testimonials_testimonials_name_item_2" id="testimonials_testimonials_name_item_2" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_item_2' ); ?>">
                    <input class="wp-testimonials-input" placeholder="Title (Optional)" type="text" name="testimonials_testimonials_name_title_2" id="testimonials_testimonials_name_title_item_2" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_title_2' ); ?>">
                </div>
            </div>

            <div class="wp-testimonials-information-box">
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_3">Quote</label>
                    <textarea class="wp-testimonials-textarea" name="testimonials_testimonials_quote_item_3"><?php echo testimonials_get_meta( 'testimonials_testimonials_quote_item_3' ); ?></textarea>
                </div>
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_3"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-testimonials-input" type="text" name="testimonials_testimonials_name_item_3" id="testimonials_testimonials_name_item_3" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_item_3' ); ?>">
                    <input class="wp-testimonials-input" placeholder="Title (Optional)" type="text" name="testimonials_testimonials_name_title_3" id="testimonials_testimonials_name_title_item_3" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_title_3' ); ?>">
                </div>
            </div>

            <div class="wp-testimonials-information-box">
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_4">Quote</label>
                    <textarea class="wp-testimonials-textarea" name="testimonials_testimonials_quote_item_4"><?php echo testimonials_get_meta( 'testimonials_testimonials_quote_item_4' ); ?></textarea>
                </div>
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_4"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-testimonials-input" type="text" name="testimonials_testimonials_name_item_4" id="testimonials_testimonials_name_item_4" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_item_4' ); ?>">
                    <input class="wp-testimonials-input" placeholder="Title (Optional)" type="text" name="testimonials_testimonials_name_title_4" id="testimonials_testimonials_name_title_item_4" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_title_4' ); ?>">
                </div>
            </div>

            <div class="wp-testimonials-information-box">
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_5">Quote</label>
                    <textarea class="wp-testimonials-textarea" name="testimonials_testimonials_quote_item_5"><?php echo testimonials_get_meta( 'testimonials_testimonials_quote_item_5' ); ?></textarea>
                </div>
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_5"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-testimonials-input" type="text" name="testimonials_testimonials_name_item_5" id="testimonials_testimonials_name_item_5" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_item_5' ); ?>">
                    <input class="wp-testimonials-input" placeholder="Title (Optional)" type="text" name="testimonials_testimonials_name_title_5" id="testimonials_testimonials_name_title_item_5" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_title_5' ); ?>">
                </div>
            </div>

            <div class="wp-testimonials-information-box">
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_6">Quote</label>
                    <textarea class="wp-testimonials-textarea" name="testimonials_testimonials_quote_item_6"><?php echo testimonials_get_meta( 'testimonials_testimonials_quote_item_6' ); ?></textarea>
                </div>
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_6"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-testimonials-input" type="text" name="testimonials_testimonials_name_item_6" id="testimonials_testimonials_name_item_6" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_item_6' ); ?>">
                    <input class="wp-testimonials-input" placeholder="Title (Optional)" type="text" name="testimonials_testimonials_name_title_6" id="testimonials_testimonials_name_title_item_6" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_title_6' ); ?>">
                </div>
            </div>

            <div class="wp-testimonials-information-box">
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_7">Quote</label>
                    <textarea class="wp-testimonials-textarea" name="testimonials_testimonials_quote_item_7"><?php echo testimonials_get_meta( 'testimonials_testimonials_quote_item_7' ); ?></textarea>
                </div>
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_7"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-testimonials-input" type="text" name="testimonials_testimonials_name_item_7" id="testimonials_testimonials_name_item_7" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_item_7' ); ?>">
                    <input class="wp-testimonials-input" placeholder="Title (Optional)" type="text" name="testimonials_testimonials_name_title_7" id="testimonials_testimonials_name_title_item_7" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_title_7' ); ?>">
                </div>
            </div>

            <div class="wp-testimonials-information-box">
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_8">Quote</label>
                    <textarea class="wp-testimonials-textarea" name="testimonials_testimonials_quote_item_8"><?php echo testimonials_get_meta( 'testimonials_testimonials_quote_item_8' ); ?></textarea>
                </div>
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_8"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-testimonials-input" type="text" name="testimonials_testimonials_name_item_8" id="testimonials_testimonials_name_item_8" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_item_8' ); ?>">
                    <input class="wp-testimonials-input" placeholder="Title (Optional)" type="text" name="testimonials_testimonials_name_title_8" id="testimonials_testimonials_name_title_item_8" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_title_8' ); ?>">
                </div>
            </div>

            <div class="wp-testimonials-information-box">
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_9">Quote</label>
                    <textarea class="wp-testimonials-textarea" name="testimonials_testimonials_quote_item_9"><?php echo testimonials_get_meta( 'testimonials_testimonials_quote_item_9' ); ?></textarea>
                </div>
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_9"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-testimonials-input" type="text" name="testimonials_testimonials_name_item_9" id="testimonials_testimonials_name_item_9" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_item_9' ); ?>">
                    <input class="wp-testimonials-input" placeholder="Title (Optional)" type="text" name="testimonials_testimonials_name_title_9" id="testimonials_testimonials_name_title_item_9" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_title_9' ); ?>">
                </div>
            </div>

            <div class="wp-testimonials-information-box">
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_10">Quote</label>
                    <textarea class="wp-testimonials-textarea" name="testimonials_testimonials_quote_item_10"><?php echo testimonials_get_meta( 'testimonials_testimonials_quote_item_10' ); ?></textarea>
                </div>
                <div class="wp-testimonial-fieldset">
                    <label class="wp-testimonials-inline-label" for="testimonials_testimonials_name_item_10"><?php _e( 'Name', 'information' ); ?></label>
                    <input class="wp-testimonials-input" type="text" name="testimonials_testimonials_name_item_10" id="testimonials_testimonials_name_item_10" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_item_10' ); ?>">
                    <input class="wp-testimonials-input" placeholder="Title (Optional)" type="text" name="testimonials_testimonials_name_title_10" id="testimonials_testimonials_name_title_item_10" value="<?php echo testimonials_get_meta( 'testimonials_testimonials_name_title_10' ); ?>">

                </div>
            </div>

        <? }


    // Add custom meta box for publishers page only ----------------------------------
        function testimonials_add_meta_box() {
            add_meta_box(
                'testimonials-testimonials',
                __( 'Testimonials', 'testimonials' ),
                'testimonials_testimonials_html',
                'page',
                'normal',
                'default'
            );
        }


    // Save custom meta box info ---------------------------------------------
        function testimonials_testimonials_save( $post_id ) {
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
            if ( ! isset( $_POST['testimonials_testimonials_nonce'] ) || ! wp_verify_nonce( $_POST['testimonials_testimonials_nonce'], '_testimonials_testimonials_nonce' ) ) return;

            if ( isset( $_POST['testimonials_testimonials_name_item_1'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_item_1', esc_attr( $_POST['testimonials_testimonials_name_item_1'] ) );
            if ( isset( $_POST['testimonials_testimonials_quote_item_1'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_quote_item_1', esc_attr( $_POST['testimonials_testimonials_quote_item_1'] ) );
            if ( isset( $_POST['testimonials_testimonials_name_title_1'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_title_1', esc_attr( $_POST['testimonials_testimonials_name_title_1'] ) );

            if ( isset( $_POST['testimonials_testimonials_name_item_2'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_item_2', esc_attr( $_POST['testimonials_testimonials_name_item_2'] ) );
            if ( isset( $_POST['testimonials_testimonials_quote_item_2'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_quote_item_2', esc_attr( $_POST['testimonials_testimonials_quote_item_2'] ) );
            if ( isset( $_POST['testimonials_testimonials_name_title_2'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_title_2', esc_attr( $_POST['testimonials_testimonials_name_title_2'] ) );


            if ( isset( $_POST['testimonials_testimonials_name_item_3'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_item_3', esc_attr( $_POST['testimonials_testimonials_name_item_3'] ) );
            if ( isset( $_POST['testimonials_testimonials_quote_item_3'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_quote_item_3', esc_attr( $_POST['testimonials_testimonials_quote_item_3'] ) );
            if ( isset( $_POST['testimonials_testimonials_name_title_3'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_title_3', esc_attr( $_POST['testimonials_testimonials_name_title_3'] ) );


            if ( isset( $_POST['testimonials_testimonials_name_item_4'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_item_4', esc_attr( $_POST['testimonials_testimonials_name_item_4'] ) );
            if ( isset( $_POST['testimonials_testimonials_quote_item_4'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_quote_item_4', esc_attr( $_POST['testimonials_testimonials_quote_item_4'] ) );
            if ( isset( $_POST['testimonials_testimonials_name_title_4'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_title_4', esc_attr( $_POST['testimonials_testimonials_name_title_4'] ) );

            if ( isset( $_POST['testimonials_testimonials_name_item_5'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_item_5', esc_attr( $_POST['testimonials_testimonials_name_item_5'] ) );
            if ( isset( $_POST['testimonials_testimonials_quote_item_5'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_quote_item_5', esc_attr( $_POST['testimonials_testimonials_quote_item_5'] ) );
            if ( isset( $_POST['testimonials_testimonials_name_title_5'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_title_5', esc_attr( $_POST['testimonials_testimonials_name_title_5'] ) );

            if ( isset( $_POST['testimonials_testimonials_name_item_6'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_item_6', esc_attr( $_POST['testimonials_testimonials_name_item_6'] ) );
            if ( isset( $_POST['testimonials_testimonials_quote_item_6'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_quote_item_6', esc_attr( $_POST['testimonials_testimonials_quote_item_6'] ) );
            if ( isset( $_POST['testimonials_testimonials_name_title_6'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_title_6', esc_attr( $_POST['testimonials_testimonials_name_title_6'] ) );

            if ( isset( $_POST['testimonials_testimonials_name_item_7'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_item_7', esc_attr( $_POST['testimonials_testimonials_name_item_7'] ) );
            if ( isset( $_POST['testimonials_testimonials_quote_item_7'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_quote_item_7', esc_attr( $_POST['testimonials_testimonials_quote_item_7'] ) );
            if ( isset( $_POST['testimonials_testimonials_name_title_7'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_title_7', esc_attr( $_POST['testimonials_testimonials_name_title_7'] ) );

            if ( isset( $_POST['testimonials_testimonials_name_item_8'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_item_8', esc_attr( $_POST['testimonials_testimonials_name_item_8'] ) );
            if ( isset( $_POST['testimonials_testimonials_quote_item_8'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_quote_item_8', esc_attr( $_POST['testimonials_testimonials_quote_item_8'] ) );
            if ( isset( $_POST['testimonials_testimonials_name_title_8'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_title_8', esc_attr( $_POST['testimonials_testimonials_name_title_8'] ) );

            if ( isset( $_POST['testimonials_testimonials_name_item_9'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_item_9', esc_attr( $_POST['testimonials_testimonials_name_item_9'] ) );
            if ( isset( $_POST['testimonials_testimonials_quote_item_9'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_quote_item_9', esc_attr( $_POST['testimonials_testimonials_quote_item_9'] ) );
            if ( isset( $_POST['testimonials_testimonials_name_title_9'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_title_9', esc_attr( $_POST['testimonials_testimonials_name_title_9'] ) );

            if ( isset( $_POST['testimonials_testimonials_name_item_10'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_item_10', esc_attr( $_POST['testimonials_testimonials_name_item_10'] ) );
            if ( isset( $_POST['testimonials_testimonials_quote_item_10'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_quote_item_10', esc_attr( $_POST['testimonials_testimonials_quote_item_10'] ) );
            if ( isset( $_POST['testimonials_testimonials_name_title_10'] ) )
                update_post_meta( $post_id, 'testimonials_testimonials_name_title_10', esc_attr( $_POST['testimonials_testimonials_name_title_10'] ) );

        }
        add_action( 'save_post', 'testimonials_testimonials_save' );


?>