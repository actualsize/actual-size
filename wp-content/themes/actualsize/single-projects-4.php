<?php get_header(); ?>

	<h1>TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTEST</h1>
	<h1>TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTEST</h1>
	<h1>TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTEST</h1>
	<h1>TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTEST</h1>
	<h1>TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTEST</h1>

	<!-- Start the loop -->
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="single-project">

			<!-- Project title ======================================================================= -->
			<?php
				$project_title_display = get_field('project_title_display', null);
				$project_title_title = get_the_title();
				$project_title_client_name = get_field('project_title_client_name', null);

				$project_title_font_size = get_field('project_title_font_size', null);
				$project_header_text_alignment = get_field('project_header_text_alignment', null);
				$project_title_headline_font_color = get_field('project_title_headline_font_color', null);
				$project_title_client_name_font_color = get_field('project_title_client_name_font_color', null);

				$project_title_include_background_image = get_field('project_title_include_background_image', null);
				$project_title_background_image = get_field('project_title_background_image', null);
				$project_title_background_image_size = get_field('project_title_background_image_size', null);
				$project_title_background_color = get_field('project_title_background_color', null);
				$project_title_include_custom_css = get_field('project_title_include_custom_css', null);
			?>
			<?php if ($project_title_display == "Yes") { ?>
				<div class="project-title <?php echo $project_title_background_image_size; ?>" style="text-align: <?php echo $project_header_text_alignment; ?>;">
					<div class="container <?php echo $project_title_font_size; ?>">
						<h1 class="head-featured"><?php echo $project_title_title; ?></h1>
						<h2 class="client-name"><?php echo $project_title_client_name; ?></h2>
					</div>
				</div>

				<style>
					.project-title {
						background: <?php echo $project_title_background_color; ?>;
					}
					.head-featured {
						color: <?php echo $project_title_headline_font_color; ?>;
						font-family: "league-gothic", sans-serif;
					}
					.client-name { color: <?php echo $project_title_client_name_font_color; ?>; }

					<?php if ($project_title_include_background_image == "Yes") {  ?>
						.project-title {
							background-image: url(<?php echo $project_title_background_image; ?>);
							background-position: center center;
						}
						.project-title.Cover { background-size: cover; }
						.project-title.Center { background-repeat: no-repeat; }
					<?php } ?>

					<?php if ($project_title_include_custom_css !== null) { ?>
						.project-title {
							<?php echo $project_title_include_custom_css; ?>
						}
					<?php } ?>
				</style>
			<?php } ?>


			<!-- Sections ============================================================================ -->
			<?php if (have_rows('section')): ?>
				<?php $section_number = -1; ?>
				<?php while( have_rows('section') ): the_row(); ?>

					<!-- Get number for identifying section variables -->
					<?php
						$section_number++;
						$section = 'section_' . $section_number . '_';
						$section_class = 'section-' . $section_number;
					?>

					<!-- Get the section type (text, slider, video, etc...) -->
					<?php $section_type = get_field($section . 'section_type', null); ?>

					<!-- Text ============================================================================ -->
						<?php if ($section_type == "Text") { ?>
							<?php
								$text_section_headline = get_field($section . 'text_section_headline', null);
								$text_section_paragraph_text_color = get_field($section . 'text_section_paragraph_text_color', null);
								$text_section_headline_color = get_field($section . 'text_section_headline_color', null);
								$text_section_headline_font_size = get_field($section . 'text_section_headline_font_size', null);
								$text_section_paragraph_font_size = get_field($section . 'text_section_paragraph_font_size', null);

								$text_section_column_size = get_field($section . 'text_section_column_size', null);
								$text_full_width_section_text = get_field($section . 'text_full_width_section_text', null);
								$text_left_half_section_text = get_field($section . 'text_left_half_section_text', null);
								$text_right_half_section_text = get_field($section . 'text_right_half_section_text', null);
								$text_left_two_thirds_section_text = get_field($section . 'text_left_two_thirds_section_text', null);
								$text_right_one_third_section_text = get_field($section . 'text_right_one_third_section_text', null);
								$text_include_vertical_rule = get_field($section . 'text_include_vertical_rule', null);
								$text_vertical_rule_style = get_field($section . 'text_vertical_rule_style', null);
									if ($text_include_vertical_rule == "Yes") {
										$text_vertical_rule_style = 'border-right: ' . $text_vertical_rule_style . ';';
									} else {
										$text_vertical_rule_style = '';
									}
								$text_section_top_margin = get_field($section . 'text_section_top_margin', null);
								$text_section_bottom_margin = get_field($section . 'text_section_bottom_margin', null);
								$text_section_background_color = get_field($section . 'text_section_background_color', null);
								$text_include_section_background_image = get_field($section . 'text_include_section_background_image', null);
								$text_section_background_image = get_field($section . 'text_section_background_image', null);
									if ($text_include_section_background_image == "Yes") {
										$text_section_background_image = 'background-image: url(' . $text_section_background_image . ');';
									} else {
										$text_section_background_image = '';
									}
								$text_section_background_image_layout = get_field($section . 'text_section_background_image_layout', null);
									if ($text_section_background_image_layout == "Cover") { $text_section_background_image_layout = "background-size: cover; background-position: center center;"; }
									if ($text_section_background_image_layout == "Tiled") { $text_section_background_image_layout = "background-position: center center;"; }
									if ($text_section_background_image_layout == "Center") { $text_section_background_image_layout = "background-position: center center; background-repeat: no-repeat;"; }
								$text_section_include_label = get_field($section . 'text_section_include_label', null);
								$text_section_label_text = get_field($section . 'text_section_label_text', null);
								$text_section_label_text_position = get_field($section . 'text_section_label_text_position', null);
								$text_section_label_font_size = get_field($section . 'text_section_label_font_size', null);
								$text_section_label_text_color = get_field($section . 'text_section_label_text_color', null);

								$text_section_headline_alignment = get_field($section . 'text_section_headline_alignment', null);
								$text_section_paragraph_alignment = get_field($section . 'text_section_paragraph_alignment', null);
							?>

							<section class="text <?php echo $section_class; ?>" style="background-color: <?php echo $text_section_background_color; ?>; <?php echo $text_section_background_image; ?><?php echo $text_section_background_image_layout; ?> padding-top: <?php echo $text_section_top_margin; ?>; padding-bottom: <?php echo $text_section_bottom_margin; ?>">
								<div class="container">

									<!-- Text section label, if applicable -->
									<?php if ($text_section_include_label == "Yes") { ?>
										<div class="section-label <?php echo $text_section_label_text_position; ?>" style="font-size: <?php echo $text_section_label_font_size; ?>; color: <?php echo $text_section_label_text_color; ?>; font-size: <?php echo $text_section_label_font_size; ?>">
											<?php echo $text_section_label_text; ?>
										</div>
									<?php } ?>

									<!-- Section headline, if applicable -->
									<?php if ($text_section_headline !== '') { ?>
										<h2 class="headline <?php echo $text_section_headline_font_size; ?>" style="text-align: <?php echo $text_section_headline_alignment; ?>; color: <?php echo $text_section_headline_color; ?>">
											<?php echo $text_section_headline; ?>
										</h2>
									<?php } ?>

									<!-- One column, full width paragraph text -->
									<?php if ($text_section_column_size == "full") { ?>
										<div class="one-column <?php echo $text_section_paragraph_font_size; ?>" style="text-align: <?php echo $text_section_paragraph_alignment; ?>; color: <?php echo $text_section_paragraph_text_color; ?>">
											<?php echo $text_full_width_section_text; ?>
										</div>
									<?php } ?>

									<!-- Half | half paragraph paragraph text-->
									<?php if ($text_section_column_size == "half") { ?>
										<div class="half-column">
											<div class="left <?php echo $text_section_paragraph_font_size; ?>" style="text-align: <?php echo $text_section_paragraph_alignment; ?>; color: <?php echo $text_section_paragraph_text_color; ?>; <?php echo $text_vertical_rule_style; ?>">
												<?php echo $text_left_half_section_text; ?>
											</div>
											<div class="right <?php echo $text_section_paragraph_font_size; ?>" style="text-align: <?php echo $text_section_paragraph_alignment; ?>; color: <?php echo $text_section_paragraph_text_color; ?>;">
												<?php echo $text_right_half_section_text; ?>
											</div>
										</div>
									<?php } ?>

									<!-- Two-thirds | One-third paragraph text -->
									<?php if ($text_section_column_size == "third") { ?>
										<div class="thirds-column">
											<div class="left <?php echo $text_section_paragraph_font_size; ?>" style="text-align: <?php echo $text_section_paragraph_alignment; ?>; color: <?php echo $text_section_paragraph_text_color; ?>; <?php echo $text_vertical_rule_style; ?>">
												<?php echo $text_left_two_thirds_section_text; ?>
											</div>
											<div class="right <?php echo $text_section_paragraph_font_size; ?>" style="text-align: <?php echo $text_section_paragraph_alignment; ?>; color: <?php echo $text_section_paragraph_text_color; ?>;">
												<?php echo $text_right_one_third_section_text; ?>
											</div>
										</div>
									<?php } ?>
								</div>
							</section>
			      <?php } ?>


					<!-- Slider ===========================================================================-->
						<?php if ($section_type == "Slider") { ?>
							<?php
								$slider_images = get_field($section . 'slider_images', null);
								$slider_height = get_field($section . 'slider_height', null);
								$slider_options = get_field($section . 'slider_options', null);
								$slider_top_margin = get_field($section . 'slider_top_margin', null);
								$slider_bottom_margin = get_field($section . 'slider_bottom_margin', null);
								$slider_images_margin_leftright = get_field($section . 'slider_images_margin_leftright', null);
								$slider_background_color = get_field($section . 'slider_background_color', null);
								$slider_containment = get_field($section . 'slider_containment', null);
							?>
							<?php if ($slider_images) { ?>
								<style>
									.three-hundred-px .carousel-cell { height: 300px; }
									.five-hundred-px .carousel-cell { height: 500px; }
									.eight-hundred-px .carousel-cell { height: 800px; }
									.viewport-height .carousel-cell {
										height: 100vh;
										min-height: 300px;
									}
									.half-viewport-height .carousel-cell {
										height: 50vh;
										min-height: 300px;
									}
								</style>

								<section class="<?php echo $slider_containment; ?> <?php echo $section_class; ?>" style="background-color: <?php echo $slider_background_color; ?>; padding-top: <?php echo $slider_top_margin; ?>; padding-bottom: <?php echo $slider_bottom_margin; ?>;">
									<div class="main-carousel js-flickity <?php echo $slider_height; ?>" data-flickity='<?php echo $slider_options; ?>'>
										<!-- Loop through gallery images -->
										<?php foreach($slider_images as $image): ?>
					            		<div class="carousel-cell" style="margin-left: <?php echo $slider_images_margin_leftright ?>; margin-right: <?php echo $slider_images_margin_leftright ?>;">
								            <img src="<?php echo $image['sizes']['full']; ?>" alt="<?php echo $image['caption']; ?>" />
											</div>
					        			<?php endforeach; ?>
					      		</div>
				      		</section>
			         	<?php } ?>
						<?php } ?>


					<!-- Image(s) =========================================================================-->
						<?php if ($section_type == "Image(s)") { ?>
							<?php
								$images = get_field($section . 'images', null);
								$images_background_color = get_field($section . 'images_background_color', null);
								$images_containment = get_field($section . 'images_containment', null);
								$images_top_margin = get_field($section . 'images_top_margin', null);
								$images_bottom_margin = get_field($section . 'images_bottom_margin', null);
								$images_include_caption = get_field($section . 'images_include_caption', null);
								$image_caption_position = get_field($section . 'image_caption_position', null);
								$image_caption_color = get_field($section . 'image_caption_color', null);
							?>
							<?php if ($images) { ?>
				      		<section class="images-gallery <?php echo $section_class; ?>" style="background-color: <?php echo $images_background_color; ?>; padding-top: <?php echo $images_top_margin; ?>; padding-bottom: <?php echo $images_bottom_margin; ?>;">
				      			<div class="<?php echo $images_containment; ?>">
					      			<?php foreach($images as $image): ?>
					      				<?php
					      					$width = $image['width'];
					      					$height = $image['height'];
						      				$ratio = $height / $width * 100;

					      					if (count($images) == 1) {
					      						$count = "single";
					      					} else {
					      						$count = "double";
					      					}
					      				?>

							        	<div class="image <?php echo $count; ?>" style="background-image: url(<?php echo $image['sizes']['full']; ?>);" alt="<?php echo $image['caption']; ?>">
								        	<div class="spacer" style="padding-bottom: <?php echo $ratio; ?>%"></div>

								         	<?php if ($images_include_caption == "Yes") { ?>
								         		<div class="caption <?php echo $image_caption_position; ?>" style="color: <?php echo $image_caption_color; ?>;"><?php echo $image['caption']; ?></div>
								         	<?php } ?>
								        </div>
					        		<?php endforeach; ?>
					        	</div>
				      		</section>
			         	<?php } ?>
						<?php } ?>


					<!-- Video ============================================================================-->
						<?php if ($section_type == "Video (Vimeo)") { ?>
							<?php
								$vimeo_video_url = get_field($section . 'vimeo_video_url', null);
								$vimeo_video_top_margin = get_field($section . 'vimeo_video_top_margin', null);
								$vimeo_video_bottom_margin = get_field($section . 'vimeo_video_bottom_margin', null);
								$vimeo_loop_video = get_field($section . 'vimeo_video_loop_video', null);
									if ($vimeo_loop_video == "Yes") {
										$vimeo_loop_video = 1;
									} else {
										$vimeo_loop_video = 0;
									}
								$vimeo_autoplay_video = get_field($section . 'vimeo_autoplay_video', null);
									if ($vimeo_autoplay_video == "Yes") {
										$vimeo_autoplay_video = 1;
									} else {
										$vimeo_autoplay_video = 0;
									}
								$vimeo_video_containment = get_field($section . 'vimeo_video_containment', null);

								$vimeo_video_background_color = get_field($section . 'vimeo_video_background_color', null);
								$section_0_vimeo_video_button_color = get_field($section . 'vimeo_video_button_color', null);
							?>
							<section class="<?php echo $vimeo_video_containment; ?> vimeo-video <?php echo $section_class; ?>">
								<div style="position: relative; padding-bottom: 56.25%; width: 100%; margin-top: <?php echo $vimeo_video_top_margin; ?>; margin-bottom: <?php echo $vimeo_video_bottom_margin; ?>;">
									<iframe src="//player.vimeo.com/video/<?php echo str_replace('https://vimeo.com/','', $vimeo_video_url); ?>?color=<?php echo str_replace('#','', $section_0_vimeo_video_button_color); ?>&loop=<?php echo $vimeo_loop_video; ?>&autoplay=<?php echo $vimeo_autoplay_video; ?>" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" width="4000" height="3000" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					        	</div>
							</section>
						<?php } ?>


					<!-- Horizontal Rule ===================================================================-->
						<?php if ($section_type == "Horizontal rule") { ?>
							<?php
								$horizontal_rule_bottom_margin = get_field($section . 'horizontal_rule_bottom_margin', null);
								$horizontal_rule_top_margin = get_field($section . 'horizontal_rule_top_margin', null);
								$horizontal_rule_style = get_field($section . 'horizontal_rule_style', null);
							?>

							<div class="<?php echo $section_class; ?>" style="margin-top: <?php echo $horizontal_rule_top_margin; ?>; margin-bottom: <?php echo $horizontal_rule_bottom_margin; ?>; border-top: <?php echo $horizontal_rule_style; ?>;" class="hr"></div>
						<?php } ?>


					<!-- Image with text ==================================================================-->
						<?php if ($section_type == "Image with text") { ?>
							<?php
								$image_with_text_layout = get_field($section . 'image_with_text_layout', null);
								$image_with_text_containment = get_field($section . 'image_with_text_containment', null);
									if ($image_with_text_containment == 'container') {
										$image_with_text_containment = 'wrapper';
									}
								$image_with_text_image = get_field($section . 'image_with_text_image', null);
								$image_with_text_section_background_color = get_field($section . 'image_with_text_section_background_color', null);
								$image_with_text_text_color = get_field($section . 'image_with_text_text_color', null);
								$image_with_text_text_alignment = get_field($section . 'image_with_text_text_alignment', null);
								$image_with_text_top_margin = get_field($section . 'image_with_text_top_margin', null);
								$image_with_text_bottom_margin = get_field($section . 'image_with_text_bottom_margin', null);
								$image_with_text_headline = get_field($section . 'image_with_text_headline', null);
								$image_with_text_paragraph = get_field($section . 'image_with_text_paragraph', null);
								$image_with_text_section_font_size = get_field($section . 'image_with_text_section_font_size', null);
								$include_image_with_text_section_label = get_field($section . 'include_image_with_text_section_label', null);
								$image_with_text_section_label_text = get_field($section . 'image_with_text_section_label_text', null);
								$image_with_text_section_label_text_position = get_field($section . 'image_with_text_section_label_text_position', null);
								$image_with_text_section_label_font_size = get_field($section . 'image_with_text_section_label_font_size', null);
								$image_with_text_section_label_text_color = get_field($section . 'image_with_text_section_label_text_color', null);
								$image_with_text_section_author_include = get_field($section . 'image_with_text_section_author_include', null);
								$image_with_text_section_author_name = get_field($section . 'image_with_text_section_author_name', null);
								$image_with_text_section_author_image = get_field($section . 'image_with_text_section_author_image', null);
							?>

							<section class="image-with-text <?php echo $image_with_text_layout; ?> <?php echo $section_class; ?>" style="background-color: <?php echo $image_with_text_section_background_color; ?>; color: <?php echo $image_with_text_text_color; ?>; text-align: <?php echo $image_with_text_text_alignment; ?>; padding-top: <?php echo $image_with_text_top_margin; ?>; padding-bottom: <?php echo $image_with_text_bottom_margin; ?>;">
								<div class="<?php echo $image_with_text_containment; ?>">
									<!-- Text section label, if applicable -->
									<?php if ($include_image_with_text_section_label == "Yes") { ?>
										<div class="section-label wrapper <?php echo $image_with_text_section_label_text_position; ?>" style="font-size: <?php echo $image_with_text_section_label_font_size; ?>; color: <?php echo $image_with_text_section_label_text_color; ?>; font-size: <?php echo $image_with_text_section_label_font_size; ?>">
											<?php echo $image_with_text_section_label_text; ?>
										</div>
									<?php } ?>
								</div>
								<div class="<?php echo $image_with_text_containment; ?>">
									<!-- Image left, text right layout -->
									<?php if ($image_with_text_layout == "image-left") { ?>
										<div class="image-box"><img src="<?php echo $image_with_text_image['sizes']['large']; ?>" /></div><div class="text-box <?php echo $image_with_text_section_font_size; ?>">
											<?php if($image_with_text_section_author_include == "Yes") { ?>
												<div class="author-box">
													<img width="40" height="40" src="<?php echo $image_with_text_section_author_image['sizes']['team-sm']; ?>">
													<?php echo $image_with_text_section_author_name; ?>
												</div>
											<?php } ?>
											<?php if($image_with_text_headline !== '') { ?>
												<h2><?php echo $image_with_text_headline; ?></h2>
											<?php } ?>
											<?php if($image_with_text_paragraph !== '') { ?>
												<p><?php echo $image_with_text_paragraph; ?></p>
											<?php } ?>
										</div>
									<?php } ?>

									<!-- Text left, image right layout -->
									<?php if ($image_with_text_layout == "image-right") { ?>
										<div class="text-box <?php echo $image_with_text_section_font_size; ?>">
											<?php if($image_with_text_section_author_include == "Yes") { ?>
												<div class="author-box">
													<img width="30" height="30" src="<?php echo $image_with_text_section_author_image['sizes']['team-sm']; ?>">
													<?php echo $image_with_text_section_author_name; ?>
												</div>
											<?php } ?>
											<?php if($image_with_text_headline !== '') { ?>
												<h2><?php echo $image_with_text_headline; ?></h2>
											<?php } ?>
											<?php if($image_with_text_paragraph !== '') { ?>
												<p><?php echo $image_with_text_paragraph; ?></p>
											<?php } ?>
										</div><div class="image-box"><img src="<?php echo $image_with_text_image['sizes']['large']; ?>" /></div>
									<?php } ?>
								</div>
							</section>
						<?php } ?>


						<?php
							$text_section_custom_css = get_field($section . 'text_section_custom_css', null);
							$images_section_custom_css = get_field($section . 'images_section_custom_css', null);
							$vimeo_video_custom_css = get_field($section . 'vimeo_video_custom_css', null);
							$image_with_text_custom_css = get_field($section . 'image_with_text_custom_css', null);
							$slider_section_custom_css = get_field($section . 'slider_section_custom_css', null);
						?>

						<style>
							.<?php echo $section_class; ?> {
								<?php
									if ($section_type == "Text") {
										echo $text_section_custom_css;
									}
									if ($section_type == "Image(s)") {
										echo $images_section_custom_css;
									}
									if ($section_type == "Video (Vimeo)") {
										echo $vimeo_video_custom_css;
									}
									if ($section_type == "Image with text") {
										echo $image_with_text_custom_css;
									}
									if ($section_type == "Slider") {
										echo $slider_section_custom_css;
									}
								?>
							}
						</style>





				<?php endwhile; ?><!-- End while( have_rows('section') ) -->
			<?php endif; ?><!-- End if (have_rows('section')) -->

			<!-- Project Conclusions ================================================================= -->
				<?php
					$conclusions_display = get_field('include_conclusions_section', null);
					$conclusions_label = get_field('conclusions_label', null);
					$project_title_client_name = get_field('project_title_client_name', null);
					$conclusions_text_color = get_field('conclusions_text_color', null);
					$conclusions_background_color = get_field('conclusions_background_color', null);
					$conclusions_margin_top = get_field('conclusions_margin_top', null);
					$conclusions_margin_bottom = get_field('conclusions_margin_bottom', null);
				?>

				<?php if ($project_title_display == "Yes") { ?>
					<div class="project-conclusions" style="padding-top: <?php echo $conclusions_margin_top; ?>; padding-bottom: <?php echo $conclusions_margin_bottom; ?>; background-color: <?php echo $conclusions_background_color; ?>; color: <?php echo $conclusions_text_color; ?>;">
						<div class="container">
							<h2>
								<?php echo $conclusions_label; ?>
							</h2>

							<?php if (have_rows('conclusions')): ?>
								<?php $conclusion_number = -1; ?>
								<?php while( have_rows('conclusions') ): the_row(); ?>

									<!-- Get number for identifying section variables -->
									<?php $conclusion_number++; ?>
									<?php $conclusion = 'conclusions_' . $conclusion_number . '_' ?>

										<?php
											$conclusions_author_image = get_field($conclusion . 'conclusions_author_image', null);
											$conclusions_author_name = get_field($conclusion . 'conclusion_author_name', null);
											$conclusion_headline = get_field($conclusion . 'project_conclusion_headline', null);
											$conclusions_body = get_field($conclusion . 'project_conclusion_body', null);
										?>

										<div class="conclusion">
											<div class="conclusion-author">
												<div class="author-box">
													<?php echo $conclusions_author_name; ?><img width="30" height="30" src="<?php echo $conclusions_author_image['sizes']['team-sm']; ?>">
												</div>
											</div><div class="conclusion-body">
												<h3 class="conclusion-headline"><?php echo $conclusion_headline; ?></h3>
												<p><?php echo $conclusions_body; ?></p>
											</div>
										</div>


								<?php endwhile; ?><!-- End while( have_rows('section') ) -->
							<?php endif; ?><!-- End if (have_rows('section')) -->


						</div>
					</div>
				<?php } ?>

		</div><!-- .single-project -->

	<?php endwhile; endif; ?><!-- End while (have_posts()); End if (have_posts()) -->


<script src="/wp-content/themes/actualsize/_/js/flickity.pkgd.min.js"></script>

<?php get_footer(); ?>