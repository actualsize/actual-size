<?php

/* 
Template Name: Contact
*/

?>
<?php get_header(); ?>
<div class="bgGrey">
	<section class="work-with-us">
		<h1 class="head-srf a lined shallow"><i class="txt">Let’s Talk About “Us”</i></h1>
		
		<form id="form1" name="form1" class="wufoo topLabel page contact-form" autocomplete="off" enctype="multipart/form-data" method="post" action="https://actualsize.wufoo.com/forms/z1n7qbl21595vom/#public">
			<fieldset>
  				<h2>First, tell us a bit about yourself:</h2>
  				<label class="desc" id="title1" for="Field1">Name<span id="req_1" class="req">*</span></label>
  				
  				<div class="group">
  					<div class="input-col input-col-half">
						<label for="Field1" class="visuallyhidden">First</label>
						<input id="Field1" name="Field1" type="text" class="field text fn" value="" size="8" tabindex="1" placeholder="First" autofocus required>
					</div><!--/.input-col-half-->
					<div class="input-col input-col-half">
						<label for="Field2" class="visuallyhidden">Last</label>
						<input id="Field2" name="Field2" type="text" class="field text ln" value="" size="14" tabindex="2" placeholder="Last" required>
					</div><!--/.input-col-half-->
				</div><!--/.group-->
				
				<label class="desc" id="title3" for="Field3">Email<span id="req_3" class="req">*</span></label>
  				<input id="Field3" name="Field3" type="email" spellcheck="false" class="field text medium" value="" maxlength="255" tabindex="3" placeholder="name@myemail.com" required> 
  				
  				<label class="desc" id="title4" for="Field4">Phone</label>
  				<div class="group">
  					<div class="input-col input-col-third">
						<input id="Field4" name="Field4" type="tel" class="field text input-tel" value="" size="3" maxlength="3" tabindex="4" placeholder="555">
					</div><!--/.input-col-third-->
					<div class="input-col input-col-third">
						<input id="Field4-1" name="Field4-1" type="tel" class="field text input-tel" value="" size="3" maxlength="3" tabindex="5" placeholder="555">
					</div><!--/.input-col-third-->
					<div class="input-col input-col-third">
						<input id="Field4-2" name="Field4-2" type="tel" class="field text input-tel" value="" size="4" maxlength="4" tabindex="6" placeholder="1234">
					</div><!--/.input-col-third-->
				</div><!--/.group-->
				
				<label class="desc" id="title5" for="Field5">Organization</label>
  				<input id="Field5" name="Field5" type="text" class="field text medium" value="" maxlength="255" tabindex="7" placeholder="My Company, Inc.">
  				
  				<label class="desc" id="title18" for="Field18">Website</label>
  				<input id="Field18" name="Field18" type="text" class="field text medium" value="" maxlength="255" tabindex="8" placeholder="www.mywebsite.com"> 
  			</fieldset>
				
			<fieldset>
					<h2>Now, let’s hear about your awesome project:</h2>
	  				<label class="desc" id="title13" for="Field13">Project Overview</label>
	  				<textarea id="Field13" name="Field13" class="field textarea medium" spellcheck="true" rows="10" tabindex="9" placeholder="Let us know what your project is all about."></textarea>
	  				
					<label class="desc" id="title9" for="Field9">Budget</label>
					<select id="Field9" name="Field9" class="field select medium input-select" tabindex="10" > 
						<option value="" selected="selected"></option>
						<option value="Over $30,000">Over $30,000</option>
						<option value="$15,000-$30,000">$15,000-$30,000</option>
						<option value="$5,000-$15,000">$5,000-$15,000</option>
						<option value="Under $5,000">Under $5,000</option>
						<option value="I&#039;m Not Sure">I'm Not Sure</option>
					</select>
				
				
					<label class="desc" id="title11" for="Field11">Estimated timeline</label>
					<select id="Field11" name="Field11" class="field select medium input-select" tabindex="11"> 
						<option value="" selected="selected"></option>
						<option value="A few weeks.">A few weeks to a month or so</option>
						<option value="Several months.">Several months</option>
						<option value="One year or more.">One year or more</option>
						<option value="Holy crap, I need it yesterday!">Holy crap, I need it yesterday!</option>
					</select>
			</fieldset>
			
			<div class="submit-holder">
				<button class="button-block button-submit" id="saveForm" name="saveForm" type="submit">All Set, Send It&nbsp;<i class="arw"></i></button>
			</div><!--/.submit-holder-->
			
			<div class="hidden">
  				<label for="comment">Do Not Fill This Out</label>
				<textarea name="comment" id="comment" rows="1" cols="1"></textarea>
				<input type="hidden" id="idstamp" name="idstamp" value="OLfgQC0WVPSB3EQ4b+4kykvzzNTidhf5z9grkOrV9Pk=">
			</div><!--/.hidden-->
		</form> 
	</section><!--/.work-with-us-->
</div><!--/.bgGrey-->	

<?php get_footer(); ?>
