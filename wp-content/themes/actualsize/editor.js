$(function(){
	$('[data-event="add-row"]').click(getIDs);
});

function getIDs() {

	$("table.acf-table tr.acf-row").each(function(){
		var attr = $(this).attr("data-unique-id");

		console.log("THISTHISHTIS");

		if(!$(this)[0].hasAttribute("data-unique-id")) {
			var random = 'section-' + randString(5);

			$(this).attr('data-unique-id', random);
			$(this).find('[data-name="section_unique_id"] input').val('.' + random).attr("readonly", true);

			var $textarea = $(this).find('.wp-code textarea');

			if (!$.trim($textarea.val())) {
				var val = '.' + random + ' {\n'
									+ '\t'
								+ '\n}';

				$textarea.val(val);
			}
		}
	});
}


function randString(x){
  var s = '';
  while(s.length<x&&x>0){
		var r = Math.random();
		s+= (r<0.1 ? Math.floor(r * 100) : String.fromCharCode(Math.floor(r * 26) + (r > 0.5 ? 97 : 65)));
  }

  return s;
}