<?php
/*
Plugin Name: Actual Size Custom Post Types
Plugin URI: http://www.actualsize.com
Description: A custom plugin for the Actual Size Theme, to register neccessary post types.
Author: Steve Swackhammer
Version: 1.0
Author URI: http://www.actualsize.com
*/

add_action( 'init','actual_size_custom_post_types' );

function actual_size_custom_post_types(){

	// register_post_type('projects',
	// 	array(
	// 		'label' => 'Projects',
	// 		'description' => '',
	// 		'public' => true,
	// 		'show_ui' => true,
	// 		'show_in_menu' => true,
	// 		'capability_type' => 'post',
	// 		'map_meta_cap' => true,
	// 		'hierarchical' => false,
	// 		'menu_icon'=> 'dashicons-welcome-widgets-menus',
	// 		'rewrite' => array('slug' => 'work', 'with_front' => 1),
	// 		'query_var' => true,
	// 		'supports' => array('title','editor','excerpt','trackbacks','custom-fields','revisions','thumbnail','author','page-attributes','post-formats'),
	// 		'taxonomies' => array('category'),
	// 		'labels' => array (
	// 		  'name' => 'Projects',
	// 		  'singular_name' => 'Project',
	// 		  'menu_name' => 'Projects',
	// 		  'add_new' => 'Add Project',
	// 		  'add_new_item' => 'Add New Project',
	// 		  'edit' => 'Edit',
	// 		  'edit_item' => 'Edit Project',
	// 		  'new_item' => 'New Project',
	// 		  'view' => 'View Project',
	// 		  'view_item' => 'View Project',
	// 		  'search_items' => 'Search Projects',
	// 		  'not_found' => 'No Projects Found',
	// 		  'not_found_in_trash' => 'No Projects Found in Trash',
	// 		  'parent' => 'Parent Project',
	// 		)
	// 	)
	// );

	register_post_type('projects',
		array(
			'label' => 'Projects',
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => false,
			'menu_icon'=> 'dashicons-welcome-widgets-menus',
			'rewrite' => array('slug' => 'work', 'with_front' => 1),
			'query_var' => true,
			'supports' => array('title','revisions'),
			'taxonomies' => array('category'),
			'labels' => array (
			  'name' => 'New Project',
			  'singular_name' => 'Project',
			  'menu_name' => 'Projects',
			  'add_new' => 'Add Project',
			  'add_new_item' => 'Add New Project',
			  'edit' => 'Edit',
			  'edit_item' => 'Edit Project',
			  'new_item' => 'New Project',
			  'view' => 'View Project',
			  'view_item' => 'View Project',
			  'search_items' => 'Search Projects',
			  'not_found' => 'No Projects Found',
			  'not_found_in_trash' => 'No Projects Found in Trash',
			  'parent' => 'Parent Project',
			)
		)
	);

	register_post_type('clients',
		array(
			'label' => 'Clients',
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => false,
			'menu_icon'=> 'dashicons-businessman',
			'rewrite' => array('slug' => 'clients', 'with_front' => true),
			'query_var' => true,
			'supports' => array('title','editor','trackbacks','thumbnail','page-attributes','post-formats'),
			'labels' => array (
			  'name' => 'Clients',
			  'singular_name' => 'Client',
			  'menu_name' => 'Clients',
			  'add_new' => 'Add Client',
			  'add_new_item' => 'Add New Client',
			  'edit' => 'Edit',
			  'edit_item' => 'Edit Client',
			  'new_item' => 'New Client',
			  'view' => 'View Client',
			  'view_item' => 'View Client',
			  'search_items' => 'Search Clients',
			  'not_found' => 'No Clients Found',
			  'not_found_in_trash' => 'No Clients Found in Trash',
			  'parent' => 'Parent Client',
			)
		)
	);

	register_post_type('collections',
		array(
			'label' => 'Collections',
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => false,
			'menu_icon'=> 'dashicons-portfolio',
			'rewrite' => array('slug' => 'collections', 'with_front' => true),
			'query_var' => true,
			'supports' => array('title','editor','trackbacks','thumbnail','page-attributes','post-formats'),
			'labels' => array (
			  'name' => 'Collections',
			  'singular_name' => 'Collection',
			  'menu_name' => 'Collections',
			  'add_new' => 'Add Collection',
			  'add_new_item' => 'Add New Collection',
			  'edit' => 'Edit',
			  'edit_item' => 'Edit Collection',
			  'new_item' => 'New Collection',
			  'view' => 'View Collection',
			  'view_item' => 'View Collection',
			  'search_items' => 'Search Collections',
			  'not_found' => 'No Collections Found',
			  'not_found_in_trash' => 'No Collections Found in Trash',
			  'parent' => 'Parent Collection',
			)
		)
	);

    flush_rewrite_rules();
}
