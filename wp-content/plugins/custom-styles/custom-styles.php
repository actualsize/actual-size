<?php
/*
Plugin Name: Actual Size Custom Styles
Plugin URI: http://www.actualsize.com
Description: A custom plugin for the Actual Size theme, to pull custom field values into an embedded style for art directed project pages. Dependent on specific Advanced Custom Field configuration. 
Author: Steve Swackhammer
Version: 1.0
Author URI: http://www.actualsize.com
*/
load_plugin_textdomain('custom-styles', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
add_action('wp_head', 'custom_style');

function custom_style($data) {
    global $post;
    $data = null;
    
    if( is_single() && (get_post_type() == 'clients')){ 
		if(function_exists( 'get_field')){
	    	$headBgColor	= get_field('client_head_background_color', $post->ID);
	    	$headBgImg 		= get_field('client_header_background_image', $post->ID);
	    	$headBgRepeat 	= get_field('client_header_background_repeat', $post->ID);
	    	$headBg2x 		= get_field('client_header_background_2x', $post->ID);
	    	$headBrdColor	= get_field('client_header_border_color', $post->ID);
	    	$headTxtColor	= get_field('client_header_text_color', $post->ID);
	    	$highlight		= get_field('client_highlight_color', $post->ID);
	    	
	    	$bodyBgColor	= get_field('client_page_background_color', $post->ID);
	    	$bodyBgImg 		= get_field('client_page_background_image', $post->ID);
	    	$bodyBgRepeat 	= get_field('client_page_background_image_repeat', $post->ID);
	    	$bodyBg2x 		= get_field('client_page_background_2x', $post->ID);
	    	$bodyTxt		= get_field('client_body_text_color', $post->ID);
	    	
	    	if(!$headBgImg){
		    	$headBg = $headBgColor.';';
	    	} else {
		    	$headBg = $headBgColor.' url("'.$headBgImg.'") '.$headBgRepeat.';';
		    	
		    	if($headBg2x == 'yes'){
			    	$headBgImgSize = getimagesize($headBgImg);
		    		$headBg .= 'background-size:'.(ceil($headBgImgSize[0] / 2)).'px auto;';	
		    	}
	    	}
	    	
	    	if(!$bodyBgImg){
		    	$bodyBg = $bodyBgColor.';';
	    	} else {
		    	$bodyBg = $bodyBgColor.' url("'.$bodyBgImg.'") '.$bodyBgRepeat.';';
		    	
		    	if($bodyBg2x == 'yes'){
			    	$bodyBgImgSize = getimagesize($bodyBgImg);
		    		$bodyBg .= 'background-size:'.(ceil($bodyBgImgSize[0] / 2)).'px auto;';	
		    	}
	    	}
	    	
	    	$data .=
'<style>
	.client-header {
		background: '.$headBg.'
		border-color: '.$headBrdColor.' !important;
	}
	
	.client-header h1,
	.client-header .desc{
		color: '.$headTxtColor.';;
	}
	
	.project-img .caption,
	.project-detail .head-block,	
	.project-detail .head-block a{
		color: '.$highlight.';
	}
	.view-project{
		background-color: '.$highlight.';
	}
	
	.imgs,
	.project-img,
	.project-detail,
	.project-info.per-client{
		border-color: '.$highlight.' !important;
	}
	
	.client{
		background: '.$bodyBg.'
	}
	
	.project-detail .desc,
	.project-detail .button-block{
		color: '.$bodyTxt.';
	}
	
	@media only screen and (min-width: 768px) {
		.view-project{
			background-color: transparent;
			color: '.$highlight.';
		}
		
		.view-project:hover{
			color: '.$highlight.' !important;
		}
	}

</style>';
			
			echo $data;
	    }
	    
    } elseif(is_single() && (get_post_type() == 'projects')){ 
    	
    
    	if(function_exists( 'get_field')){  // Plugin depends on Advanced Custom Fields
    		$heroHasColor	= get_field('separate_hero_background_color', $post->ID);
    		$heroBgColor = 	'transparent';
	    	
	    	
	    	$bgColor 		= get_field('page_background_color', $post->ID);
	    	$bgImg 			= get_field('page_background_image', $post->ID);
	    	$bgRepeat 		= get_field('background_image_repeat', $post->ID);
	    	$bg2x 			= get_field('page_background_2x', $post->ID);
	    	
	    	$titleBgColor	= get_field('project_title_background_color', $post->ID);
	    	$titleBgImg		= get_field('project_title_background_image', $post->ID);
	    	$titleBgRepeat	= get_field('project_title_background_image_repeat', $post->ID);
	    	$titleBg2x		= get_field('title_background_2x', $post->ID);
	    	$titleBrdrTop	= get_field('project_title_border_top_color', $post->ID);
	    	$titleBrdrBtm	= get_field('project_title_border_bottom_color', $post->ID);
	    	$titleTxt		= get_field('project_title_text_color', $post->ID);
	    	
	    	$txtColor 		= get_field('body_text_color', $post->ID); 
	    	$highlight  	= get_field('highlight_color', $post->ID); 
	    	$buttonBg 		= get_field('view_client_button_color', $post->ID);
	    	$buttonBgHvr	= get_field('view_client_button_color_hover', $post->ID);
	    	$buttonTxt 		= get_field('view_client_button_text', $post->ID);
	    	$buttonTxtHvr  	= get_field('view_client_button_text_hover', $post->ID);
	    	$clientHead 	= get_field('more_from_client_text_color', $post->ID);
	    	$subHead 		= get_field('what_we_did_header_text_color', $post->ID);
	    	
	    	if(!$bgImg){
		    	$projectBg = $bgColor.';';
	    	} else {
		    	$projectBg = $bgColor.' url("'.$bgImg.'") '.$bgRepeat.';';
		    	
		    	if($bg2x == 'yes'){
			    	//$projectBg .= 'background-size:'.(ceil(getimagesize($bgImg)[0] / 2)).'px auto;';	
			    	$bgImgSize = getimagesize($bgImg);
		    		$projectBg .= 'background-size:'.(ceil($bgImgSize[0] / 2)).'px auto;';	
		    	}
	    	}
	    	
	    	if($heroHasColor && $heroHasColor =='yes'){
		    	$heroBgColor	= get_field('hero_background_color', $post->ID);
	    	} 
	    	
	    	
	    	if(!$titleBgImg){
		    	$titleBg = $titleBgColor.';';
	    	} else {
		    	$titleBg = $titleBgColor.' url("'.$titleBgImg.'") '.$titleBgRepeat.';';
		    	
		    	if($titleBg2x == 'yes'){
			    	$titleBgImgSize = getimagesize($titleBgImg);
			    	$titleBg .= 'background-size:'.(ceil($titleBgImgSize[0] / 2)).'px auto;';	
		    	}
	    	}
	    	
	    	$data .=
'<style>	    	
	.title-wrap .project-title, 
	.title-wrap .client,
	.nav-projects a,
	.eye{
		color: '.$titleTxt.';
	}
	
	.title-wrap{
		border-top-color: '.$titleBrdrTop.' !important;
		border-bottom-color: '.$titleBrdrBtm.' !important;
	}
	
	.project-img .caption{
		color: '.$highlight.';
	}
	
	.imgs,
	.project-img,
	.project-detail hr{
		border-color: '.$highlight.' !important;
	}
	
	.project-detail .desc,
	.what-we-did ul{
		color: '.$txtColor.';
	}
	
	.what-we-did .head-block{
		color: '.$subHead.';
	}
	
	.button-view-site{
		color: '.$highlight.';
	}
	
	.view-client{
		background: '.$buttonBg.';
		color: '.$buttonTxt.';
	}
	
	.view-client:hover{
		background: '.$buttonBgHvr.';
		color: '.$buttonTxtHvr.';
	}
	
	.client-more .head-client-more{
		color: '.$clientHead.';
	}
	
	.hero-wrap{
		background: '.$heroBgColor.';
	}
	
	.project-header .title-wrap{
		background: '.$titleBg.'
	}
	
	.project{
		background: '.$projectBg.'
	}
	
</style>';
	    	
	    	echo $data;
    	}
    
    } elseif(is_page_template('page-team.php')){ 
		if(function_exists( 'get_field')){
			$rows = get_field('rows', $post->ID);
		
			if($rows){
				$i = 1;
				$data .= '
<style>';
				foreach($rows as $row){
					$data .='
	.team-member:nth-child('.$i.'n),
	.team-member:nth-child('.$i.'n) .detail{
		background-color: '.$row['row_color'].';
	}';
					
					$i++;
				}
				$data .= '
</style>';
				echo $data;	
			}
		}
	} if( is_single() && (get_post_type() == 'collections')){ 
		if(function_exists( 'get_field')){
	    	$headBgColor	= get_field('client_head_background_color', $post->ID);
	    	$headBgImg 		= get_field('client_header_background_image', $post->ID);
	    	$headBgRepeat 	= get_field('client_header_background_repeat', $post->ID);
	    	$headBg2x 		= get_field('client_header_background_2x', $post->ID);
	    	$headBrdColor	= get_field('client_header_border_color', $post->ID);
	    	$headTxtColor	= get_field('client_header_text_color', $post->ID);
	    	$highlight		= get_field('client_highlight_color', $post->ID);
	    	
	    	$bodyBgColor	= get_field('client_page_background_color', $post->ID);
	    	$bodyBgImg 		= get_field('client_page_background_image', $post->ID);
	    	$bodyBgRepeat 	= get_field('client_page_background_image_repeat', $post->ID);
	    	$bodyBg2x 		= get_field('client_page_background_2x', $post->ID);
	    	$bodyTxt		= get_field('client_body_text_color', $post->ID);
	    	
	    	if(!$headBgImg){
		    	$headBg = $headBgColor.';';
	    	} else {
		    	$headBg = $headBgColor.' url("'.$headBgImg.'") '.$headBgRepeat.';';
		    	
		    	if($headBg2x == 'yes'){
			    	$headBgImgSize = getimagesize($headBgImg);
		    		$headBg .= 'background-size:'.(ceil($headBgImgSize[0] / 2)).'px auto;';	
		    	}
	    	}
	    	
	    	if(!$bodyBgImg){
		    	$bodyBg = $bodyBgColor.';';
	    	} else {
		    	$bodyBg = $bodyBgColor.' url("'.$bodyBgImg.'") '.$bodyBgRepeat.';';
		    	
		    	if($bodyBg2x == 'yes'){
			    	$bodyBgImgSize = getimagesize($bodyBgImg);
		    		$bodyBg .= 'background-size:'.(ceil($bodyBgImgSize[0] / 2)).'px auto;';	
		    	}
	    	}
	    	
	    	$data .=
'<style>
	.client-header {
		background: '.$headBg.'
		border-color: '.$headBrdColor.' !important;
	}
	
	.client-header h1,
	.client-header .desc{
		color: '.$headTxtColor.';;
	}
	
	.project-img .caption,
	.project-detail .head-block,
	.project-detail .head-block a{
		color: '.$highlight.';
	}
	.view-project{
		background-color: '.$highlight.';
	}
	
	.imgs,
	.project-img,
	.project-detail,
	.project-info.per-client{
		border-color: '.$highlight.' !important;
	}
	
	.client{
		background: '.$bodyBg.'
	}
	
	.project-detail .desc,
	.project-detail .button-block{
		color: '.$bodyTxt.';
	}
	
	@media only screen and (min-width: 768px) {
		.view-project{
			background-color: transparent;
			color: '.$highlight.';
		}
		
		.view-project:hover{
			color: '.$highlight.' !important;
		}
	}

</style>';
			
			if (!post_password_required()) {
				echo $data;
			}
	    }
	    
    }
}
