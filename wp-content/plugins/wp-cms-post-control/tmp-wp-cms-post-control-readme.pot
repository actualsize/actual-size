msgid ""
msgstr "MIME-Version: 1.0\n"

#. Name.
msgid "WP-CMS Post Control"
msgstr ""

#. Short description.
msgid "Hides metabox controls on the write/edit post &amp; page admin screens for each user role. Also controls autosave, trash time and saved revisions."
msgstr ""

#. Screenshot description.
msgid "The first post control admin screen where the main options are set for each user level."
msgstr ""

#. Screenshot description.
msgid "The core functions option screen, where more advanced WordPress controls are set."
msgstr ""

#. Screenshot description.
msgid "An example of a customised write/edit page - much simpler to use for all your users and clients!"
msgstr ""

#. Screenshot description.
msgid "An example of a customised write/edit post - much simpler to use for all your users and clients!"
msgstr ""

#. Found in description list item.
msgid "Post Author (if multiple)"
msgstr ""

#. Found in description list item.
msgid "Post Categories"
msgstr ""

#. Found in description list item.
msgid "Post Comments"
msgstr ""

#. Found in description list item.
msgid "Post Custom fields"
msgstr ""

#. Found in description list item.
msgid "Post Discussion"
msgstr ""

#. Found in description list item.
msgid "Post Excerpt"
msgstr ""

#. Found in description list item.
msgid "Post Featured Image (NEW WordPress 3.1+ support!)"
msgstr ""

#. Found in description list item.
msgid "Post Format"
msgstr ""

#. Found in description list item.
msgid "Post Revisions"
msgstr ""

#. Found in description list item.
msgid "Post Slug (NEW WordPress 3.1+ support!)"
msgstr ""

#. Found in description list item.
msgid "Post Tags"
msgstr ""

#. Found in description list item.
msgid "Post Trackbacks"
msgstr ""

#. Found in description list item.
msgid "NEW - Word count"
msgstr ""

#. Found in description list item.
msgid "Page Attributes"
msgstr ""

#. Found in description list item.
msgid "Page Author (ONLY if multiple)"
msgstr ""

#. Found in description list item.
msgid "Page Custom Fields"
msgstr ""

#. Found in description list item.
msgid "Page Discussion"
msgstr ""

#. Found in description list item.
msgid "Page Featured Image (NEW WordPress 3.1+ support!)"
msgstr ""

#. Found in description list item.
msgid "Page Slug (NEW WordPress 3.1+ support!)"
msgstr ""

#. Found in description list item.
msgid "Page Revisions"
msgstr ""

#. Found in description list item.
msgid "Limit the number of post and page revisions saved (updated for WordPress 3.6 and above)"
msgstr ""

#. Found in description list item.
msgid "Post/Page Media upload"
msgstr ""

#. Found in description list item.
msgid "Disable Autosave"
msgstr ""

#. Found in description list item.
msgid "Disable Post Revisions (updated for WordPress 3.6 and above)"
msgstr ""

#. Found in description list item.
msgid "NEW - Set the number of days before the trash is auto emptied by WordPress"
msgstr ""

#. Found in description paragraph.
msgid "<strong>Post Control</strong> from <a href=\"http://wp-cms.com/\">WordPress CMS Modifications</a> gives you complete control over your write options <strong>for every user level/role</strong>. It not only allows you to hide unwanted items like custom fields, trackbacks, revisions etc. but also gives you a whole lot more control over how WordPress deals with creating content."
msgstr ""

#. Found in description paragraph.
msgid "Simplify and customise the write post and page areas of WordPress to show just the controls you need. Great for de-cluttering - do you really need those pingback and trackback options, not using tags, want only administrators to be able to set categories... now you can decide what each individual level of user can see and use!"
msgstr ""

#. Found in description paragraph.
msgid "It also features other advanced configuration options like disable autosaves, limit the number of revisions saved and how many days before trash is auto-emptied by WordPress."
msgstr ""

#. Found in description paragraph.
msgid "<strong>NEW</strong> - Control number of days before trash is emptied"
msgstr ""

#. Found in description paragraph.
msgid "<strong>NEW</strong> - Remove word count on a per user basis (feature request)"
msgstr ""

#. Found in description paragraph.
msgid "<strong>NEW</strong> - Control exactly how many revisions are saved when you are amending your content or even turn off revisions. This stops your database getting clogged up and is great when you are designing a site!"
msgstr ""

#. Found in description paragraph.
msgid "You can control the display of the following post options for each core WordPress role (administrator, editor, author and contributor):"
msgstr ""

#. Found in description paragraph.
msgid "You can control the display of the following page options:"
msgstr ""

#. Found in description paragraph.
msgid "You can control the display of the following global post/page options:"
msgstr ""

#. Found in installation header.
msgid "First time install"
msgstr ""

#. Found in installation header.
msgid "Update existing install"
msgstr ""

#. Found in installation list item.
msgid "Get the latest version of this plugin at the <a href=\"http://wordpress.org/extend/plugins/wp-cms-post-control/\">official WordPress plugin directory</a>"
msgstr ""

#. Found in installation list item.
msgid "Decompress .zip file, retaining the complete, original file structure"
msgstr ""

#. Found in installation list item.
msgid "Upload the whole directory <code>wp-cms-post-control</code> and all containing files to your plugins directory - normally <code>/wp-content/plugins/</code>"
msgstr ""

#. Found in installation list item.
msgid "Activate the plugin through the 'Plugins' menu in WordPress"
msgstr ""

#. Found in installation list item.
msgid "Configure options through <code>Settings &gt; Post Control</code> and <code>Settings &gt; Post Control Core</code>"
msgstr ""

#. Found in installation paragraph.
msgid "You can use the built-in WordPress add plugin system to install Post Control once logged into your site, it's quick and easy!"
msgstr ""

#. Found in installation paragraph.
msgid "Simply search for 'WP-CMS Post Control' and you can install it straight from your WordPress admin area."
msgstr ""

#. Found in installation paragraph.
msgid "If you want to manually upload and install:"
msgstr ""

#. Found in installation paragraph.
msgid "If your server supports it, the automatic plugin update feature of WordPress is the easiest way to keep this plugin updated."
msgstr ""

#. Found in installation paragraph.
msgid "If you are updating via FTP, simply delete the entire folder called <code>wp-cms-post-control</code> from your plugins directory and replace with the newest version."
msgstr ""

#. Found in installation paragraph.
msgid "<strong>IMPORTANT</strong> When you upgrade, you may need to go to the options page and re-save your Post Control options to refresh the settings. In most cases everything will be fine, but this will ensure that if there have been any updates to WordPress the plugin will refresh and update the relevant Post Control options."
msgstr ""

#. Found in changelog list item.
msgid "2nd December 2015"
msgstr ""

#. Found in changelog list item.
msgid "Tested and verified upto WordPress 4.4"
msgstr ""

#. Found in changelog list item.
msgid "Fixed issue with post options"
msgstr ""

#. Found in changelog list item.
msgid "28th April 2015"
msgstr ""

#. Found in changelog list item.
msgid "REQUIRES WORDPRESS 3.6 OR ABOVE"
msgstr ""

#. Found in changelog list item.
msgid "Tested and verified with WordPress 4.2.1"
msgstr ""

#. Found in changelog list item.
msgid "Fixed issue with options array"
msgstr ""

#. Found in changelog list item.
msgid "11th August 2014"
msgstr ""

#. Found in changelog list item.
msgid "Tested and verified with WordPress 3.9.1"
msgstr ""

#. Found in changelog list item.
msgid "5th August 2014"
msgstr ""

#. Found in changelog list item.
msgid "24th March 2014"
msgstr ""

#. Found in changelog list item.
msgid "Tested and verified with WordPress 3.8x"
msgstr ""

#. Found in changelog list item.
msgid "25th October 2013"
msgstr ""

#. Found in changelog list item.
msgid "Improve compatibility with older versions of PHP, corrects code error from incompatible code as reported by users."
msgstr ""

#. Found in changelog list item.
msgid "22nd October 2013"
msgstr ""

#. Found in changelog list item.
msgid "New option to remove word count (as per user request)"
msgstr ""

#. Found in changelog list item.
msgid "25th August 2013"
msgstr ""

#. Found in changelog list item.
msgid "Code update required to support WordPress 3.6x revisions"
msgstr ""

#. Found in changelog list item.
msgid "10th July 2013"
msgstr ""

#. Found in changelog list item.
msgid "REQUIRES WORDPRESS 3.5 OR ABOVE"
msgstr ""

#. Found in changelog list item.
msgid "Remove legacy Flash uploader option - no longer valid or relevant"
msgstr ""

#. Found in changelog list item.
msgid "Added new control for comments on page edit screen"
msgstr ""

#. Found in changelog list item.
msgid "Changed support link to WordPress forum"
msgstr ""

#. Found in changelog list item.
msgid "10th April 2011"
msgstr ""

#. Found in changelog list item.
msgid "REQUIRES WORDPRESS 3.1 OR ABOVE"
msgstr ""

#. Found in changelog list item.
msgid "Create post and page control for custom post thumbnails/featured image meta panel"
msgstr ""

#. Found in changelog list item.
msgid "Create post and page control for slug meta panel"
msgstr ""

#. Found in changelog list item.
msgid "Create post control for format meta panel"
msgstr ""

#. Found in changelog list item.
msgid "Increased number of post revisions available as per user request to 50 (thanks!)"
msgstr ""

#. Found in changelog list item.
msgid "4th November 2010"
msgstr ""

#. Found in changelog list item.
msgid "REQUIRES WORDPRESS 3.0 OR ABOVE"
msgstr ""

#. Found in changelog list item.
msgid "Hopefully fixes update notice issues as reported by users (thanks!)"
msgstr ""

#. Found in changelog list item.
msgid "Increased number of post revisions available as per user request (thanks!)"
msgstr ""

#. Found in changelog list item.
msgid "31st July 2010"
msgstr ""

#. Found in changelog list item.
msgid "Fully tested and compatible with WordPress 3.0 post and page editors"
msgstr ""

#. Found in changelog list item.
msgid "Fixed another bug with no values set"
msgstr ""

#. Found in changelog list item.
msgid "Compatibility with WordPress 3.0 page editing"
msgstr ""

#. Found in changelog list item.
msgid "18th June 2010"
msgstr ""

#. Found in changelog list item.
msgid "LAST VERSION TO BE COMPATIBLE WITH WORDPRESS 2.9"
msgstr ""

#. Found in changelog list item.
msgid "WordPress 3.0 first pass compatibility"
msgstr ""

#. Found in changelog list item.
msgid "16th June 2010"
msgstr ""

#. Found in changelog list item.
msgid "Tested upto WordPress 3.0-RC3"
msgstr ""

#. Found in changelog list item.
msgid "Amended roles and capabilities"
msgstr ""

#. Found in changelog list item.
msgid "Fully prepared and ready for WordPress 3.0 launch, whoot!"
msgstr ""

#. Found in changelog list item.
msgid "12th June 2010"
msgstr ""

#. Found in changelog list item.
msgid "Bug that caused error messages when no options selected fixed"
msgstr ""

#. Found in changelog list item.
msgid "Introduced new revisions number control"
msgstr ""

#. Found in changelog list item.
msgid "23th March 2010"
msgstr ""

#. Found in changelog list item.
msgid "Bug that caused error messages fixed"
msgstr ""

#. Found in changelog list item.
msgid "22th March 2010"
msgstr ""

#. Found in changelog list item.
msgid "Bug hunt"
msgstr ""

#. Found in changelog list item.
msgid "Added new Core Functions sub-menu page"
msgstr ""

#. Found in changelog list item.
msgid "Added new disable autosave control"
msgstr ""

#. Found in changelog list item.
msgid "Added new disable revisions control"
msgstr ""

#. Found in changelog list item.
msgid "Added new disable flash uploader control"
msgstr ""

#. Found in changelog list item.
msgid "Added cleanup of options on delete of plugin (not deactivation)"
msgstr ""

#. Found in changelog list item.
msgid "20th March 2010"
msgstr ""

#. Found in changelog list item.
msgid "Fixed bug when values empty"
msgstr ""

#. Found in changelog list item.
msgid "Amended data sanitisation input"
msgstr ""

#. Found in changelog list item.
msgid "19th March 2010"
msgstr ""

#. Found in changelog list item.
msgid "Complete re-write of codebase = major efficiency improvements"
msgstr ""

#. Found in changelog list item.
msgid "New code eliminates all previous reported user issues"
msgstr ""

#. Found in changelog list item.
msgid "WordPress 2.9.2 compatibility updates"
msgstr ""

#. Found in changelog list item.
msgid "Introduced multi-user level controls"
msgstr ""

#. Found in changelog list item.
msgid "New remove media upload control"
msgstr ""

#. Found in changelog list item.
msgid "31st March 2009"
msgstr ""

#. Found in changelog list item.
msgid "WordPress 2.7 author control"
msgstr ""

#. Found in changelog list item.
msgid "17th December 2008"
msgstr ""

#. Found in changelog list item.
msgid "WordPress 2.7 compatibility build, re-write plugin controls to support new 'Crazy Horse' interface"
msgstr ""

#. Found in changelog list item.
msgid "Fix basic text formatting in custom message box, remove strip slashes to allow basic formatting like  and "
msgstr ""

#. Found in changelog list item.
msgid "Changed option array function for more control"
msgstr ""

#. Found in changelog list item.
msgid "Changed formatting of plugin options buttons"
msgstr ""

#. Found in changelog list item.
msgid "6th September 2008"
msgstr ""

#. Found in changelog list item.
msgid "Option to hide editor sidebar shortcuts and 'Press It' function"
msgstr ""

#. Found in changelog list item.
msgid "Remove redundant preview code"
msgstr ""

#. Found in changelog list item.
msgid "Improved formatting for message box text and title input"
msgstr ""

#. Found in changelog list item.
msgid "5th September 2008"
msgstr ""

#. Found in changelog list item.
msgid "Found potential conflict with options variables declared within a theme functions file"
msgstr ""

#. Found in changelog list item.
msgid "Conflicting PHP variables for reference - 'options' and 'newoptions'"
msgstr ""

#. Found in changelog list item.
msgid "Should solve conflicts with wrongly coded variables from other plugins/themes"
msgstr ""

#. Found in changelog list item.
msgid "4th September 2008"
msgstr ""

#. Found in changelog list item.
msgid "Fix the bug introduced in v1.02 that broke the form fields"
msgstr ""

#. Found in changelog list item.
msgid "After comments feedback, changed and documented admin control"
msgstr ""

#. Found in changelog list item.
msgid "3rd September 2008"
msgstr ""

#. Found in changelog list item.
msgid "Bug catches, may help plugin compatibility on different servers"
msgstr ""

#. Found in changelog list item.
msgid "2nd August 2008"
msgstr ""

#. Found in changelog list item.
msgid "Option to insert message panel"
msgstr ""

#. Found in changelog list item.
msgid "General tidying on admin page"
msgstr ""

#. Found in changelog list item.
msgid "1st August 2008"
msgstr ""

#. Found in changelog list item.
msgid "Option to disable post and page revisions"
msgstr ""

#. Found in changelog list item.
msgid "Option to disable autosaves"
msgstr ""

#. Found in changelog list item.
msgid "28th July 2008"
msgstr ""

#. Found in changelog list item.
msgid "Introduced Admin user control"
msgstr ""

#. Found in changelog list item.
msgid "26th July 2008"
msgstr ""

#. Found in changelog list item.
msgid "Included clean-up of database on de-activation"
msgstr ""

#. Found in changelog paragraph.
msgid ""
"1st August 2008\n"
"* Option to select uploader (Flash or standard)\n"
"* Option to hide revisions control\n"
"* Option to hide word count\n"
"* Option to hide Advanced Options header\n"
"* Fixed page custom field control\n"
"* Redesigned admin page"
msgstr ""

#. Found in faq header.
msgid "How do I delete content and not use the trash?"
msgstr ""

#. Found in faq header.
msgid "I'm using version 2.8 and get a parse error on activation and can't activate the plugin?"
msgstr ""

#. Found in faq header.
msgid "I'm using WordPress 3.6 (or above) and revisions controls don't work?"
msgstr ""

#. Found in faq header.
msgid "I'm using WordPress 3.5x and revisions controls don't work?"
msgstr ""

#. Found in faq header.
msgid "My Slug and Featured image controls don't do anything?"
msgstr ""

#. Found in faq header.
msgid "I'm using v2.x - the automatic WordPress update isn't working!"
msgstr ""

#. Found in faq header.
msgid "I'm using v2.2x with WordPress 3.0 - post options work but the page options do nothing!"
msgstr ""

#. Found in faq header.
msgid "I'm using v2.2x with WordPress 2.9 (or below) - post options and/or other things don't work!"
msgstr ""

#. Found in faq header.
msgid "I'm using v2.x and I have some error messages appear at the top of the screen."
msgstr ""

#. Found in faq header.
msgid "I used versions of this plugin prior to v2 and sometimes the controls wouldn't re-appear once deactivated."
msgstr ""

#. Found in faq header.
msgid "Can you change the options for any user role?"
msgstr ""

#. Found in faq header.
msgid "Can devious users still reveal the controls if they are hidden using tools like Firebug?"
msgstr ""

#. Found in faq header.
msgid "What options get used if I hide a control - like pingbacks and trackbacks?"
msgstr ""

#. Found in faq header.
msgid "How do I delete the option(s) out of my database permanently?"
msgstr ""

#. Found in faq header.
msgid "What happens if I activate/deactivate this plugin?"
msgstr ""

#. Found in faq header.
msgid "I installed v.2.0 and I dont have autosave and other options"
msgstr ""

#. Found in faq header.
msgid "It's not working!"
msgstr ""

#. Found in faq header.
msgid "What you got planned?"
msgstr ""

#. Found in faq header.
msgid "Wow, good work - I LOVE this plugin, and you did it all by yourself?"
msgstr ""

#. Found in faq paragraph.
msgid "In plugin version 2.9 a new control was created for setting the number of posts to keep in the trash - if you don't wish to use the trash functionality set this to 0 days."
msgstr ""

#. Found in faq paragraph.
msgid "Thanks for reporting - this is due to you having an older version of PHP on your server compared to what I'm testing on. I've made the code backwards compatible in version 2.81, so update and you should be fine!"
msgstr ""

#. Found in faq paragraph.
msgid "Update to plugin version 2.7 or above, it has been updated to support new revisions controls."
msgstr ""

#. Found in faq paragraph.
msgid "The last version of this plugin to support WordPress 3.5x is v2.6, you should think about upgrading when you can - we are ready for you!"
msgstr ""

#. Found in faq paragraph.
msgid "Your WordPress theme has to specifically support these features, if they are not enabled the controls won't enable the function. Ask your theme designer to update their theme if required!"
msgstr ""

#. Found in faq paragraph.
msgid "Please use version 2.4 and above to fix this glitch."
msgstr ""

#. Found in faq paragraph.
msgid "2.3 (and above) fixes this glitch in WordPress 3.0 - with the introduction of custom post types things have changed a bit and I have now amended the plugin to accommodate this."
msgstr ""

#. Found in faq paragraph.
msgid "2.22 was the last version with WordPRess 2.9 compatibility. If you are using an earlier version of WordPress you really should upgrade - you get lots of cool new stuff and a faster, smarter WordPress!"
msgstr ""

#. Found in faq paragraph.
msgid "2.2 (and above) fixes this glitch - thanks for the feedback!"
msgstr ""

#. Found in faq paragraph.
msgid "2.0 and above is a complete re-write using a new method to remove the controls. Because of this, these issues are now completely resolved."
msgstr ""

#. Found in faq paragraph.
msgid "<strong>YES!</strong> Administrators, editors, authors and contributors can all have different settings."
msgstr ""

#. Found in faq paragraph.
msgid "<strong>NO!</strong> All of the core controls are removed in a completely different way now - not just hidden with CSS. They can't be revealed by hacking the browser rendered CSS, as they are not even rendered to the page anywhere!"
msgstr ""

#. Found in faq paragraph.
msgid "The global options you set in the main WordPress options are used."
msgstr ""

#. Found in faq paragraph.
msgid "In v2.1 and above just deactivate the plugin and then delete it using the WordPress plugin page option (not just of your server via FTP!). When the plugin is deleted through WordPress, it also deletes the option(s) from the database options table that are created."
msgstr ""

#. Found in faq paragraph.
msgid "In v2.1 the options are set to be persistent - so if you deactivate the plugin and re-enable it, the settings will remain saved. Delete the plugin to delete the saved database options."
msgstr ""

#. Found in faq paragraph.
msgid "These are now restored in v2.1 and above. Click on the <strong>core functions</strong> link at the top of the screen, or <strong>Post Control Core</strong> in the sidebar under <strong>Settings</strong> to turn off autosaves, revisions and the Flash uploader."
msgstr ""

#. Found in faq paragraph.
msgid "<strong>Make sure you are using the latest version!</strong> V2.0+ is designed for WordPress 2.9 and above. If you are using a version older than that, you will have to use an older version of the plugin - and really should think about upgrading, this plugin is ready for you!"
msgstr ""

#. Found in faq paragraph.
msgid "Ensure you have the plugin installed in the correct directory - you should have a directory called <strong>wp-cms-post-control</strong> in your plugins directory. Inside there should be another directory called <strong>inc</strong>."
msgstr ""

#. Found in faq paragraph.
msgid "I've got quite a few things I'd like to do with this plugin, but don't hold your breath waiting for them to happen... you may burst!"
msgstr ""

#. Found in faq paragraph.
msgid "What began as inherited code has now been completely re-written in v2.0 to use new methods and best practices in WordPress plugin development by Jonny. The first codebase began as a plugin build by Brady J. Frey and I maintained this version for some time, but version 2 is a complete re-write from the ground up."
msgstr ""