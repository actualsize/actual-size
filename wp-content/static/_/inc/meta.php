<footer class="meta">
	<time datetime="<?php echo date(DATE_W3C); ?>" pubdate class="updated"><?php the_time('F jS, Y') ?></time>
	<?php //comments_popup_link('No Comments', '1 Comment', '% Comments', 'comments-link', ''); ?>
</footer>