<section>
<?php
$args=array(
	'post_type' => 'clients',
	'posts_per_page'=> 6,
	'meta_key' => 'has_detail_page',
	'meta_value' => 'yes',
	'orderby'   => 'menu_order',
    'order'     => 'ASC'
);

$clients = null;
$clients = new WP_Query($args);
		
if($clients->have_posts()) {

?>
	<h1 class="head-block full invert">Featured Clients&nbsp;<i class="arw dwn"></i></h1>
		<div class="featured-clients">
			<ul class="list-4">
<?php		
	$i = 1;
	while ($clients->have_posts()) : $clients->the_post();   
?>
				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?>&nbsp;<i class="arw"></i></a></li>	
<?php				
		if($i % 2 == 0 || $i == $clients->post_count){
?>
			</ul>
<?php				
			if($i != $clients->post_count){
?>
			<ul class="list-4">
<?php						
			}	
		} 
					
		$i++;
		
	endwhile;
?>
			<div class="full">
				<a href="<?php echo get_page_link(163); ?>#client-list" class="button-block invert">Full Client List&nbsp;<i class="arw"></i></a>
			</div>
		</div>
<?php		
		}
?>				
</section>