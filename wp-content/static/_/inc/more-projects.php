<section class="more-projects">
	<h1 class="head-block full">More Projects&nbsp;<i class="arw dwn"></i></h1>
	<div class="projects">
		<ul class="project-list sm">
<?php 
$ogID = $post->ID;
$usedIds = array();
for($i = 1; $i <= 4; $i++){
	$post = get_adjacent_post(false,'',false);
	setup_postdata($post);
	
	
	// Dear Future Me: This doesn't work quite right. 
	// I hope you are able to fix it. 
	// You might want to look into filtering get_adjacent_post()
	// to use menu_order to order its data, but I couldn't 
	// really get it to work.
	
	if($post->ID == $ogID || in_array($post->ID, $usedIds) || !get_the_title()){
		$args = array(
	        'post_type' => 'projects',
	        'posts_per_page' => 1,
	        'orderby'   => 'menu_order',
            'order'     => 'ASC'
	    );
	    
	    $items = get_posts($args);
	    if($items){
		    foreach($items as $item){
			    $post = $item;
		    }
	    }
	}
	
	$usedIds[] = $post->ID; 
	$thumb	= get_field('list_thumbnail', null);
	$thumb	= $thumb['sizes']['grid-thumb'];
	$client = get_field('client', null);    
?>
			<li>
				<a href="<?php the_permalink(); ?>">
<?php 
				if($thumb){
?>				
					<img src="<?php echo $thumb; ?>">
<?php					
				} else {
?>
					<img src="<?php bloginfo('template_directory'); ?>/img/thumb-blank.gif">
<?php					
				}
?>
					<h2 class="head-item"><?php the_title(); ?>&nbsp;<i class="arw"></i></h2>
				</a>
<?php
				$curPost = $post;
				if($client){	
					$post = $client;
					setup_postdata($post);	
					$hasDetail	= get_field('has_detail_page', $post->ID);

					if($hasDetail == 'yes'){
?>
						<a href="<?php echo the_permalink(); ?>" class="sub-item"><?php echo the_title(); ?></a>
<?php						
					} else {
?>
						<span class="sub-item"><?php echo the_title(); ?></span>
<?php						
					}	
					
					$post = $curPost;
				}
?>				
			</li>
<?php		
}

wp_reset_postdata();
?>			
		</ul><!--/.project-list .sm-->
		<a href="<?php echo get_page_link(121); ?>" class="button-block"><i class="arw lft">&nbsp;</i>Back to Projects</a>
	</div><!--/.projects-->
</section><!--/.more-projects-->